<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/team.php" />
<link rel="canonical" href="https://vincaps.com/team.php" />
<meta property="og:title" content="Vincaps Capital <?php echo _HEADER_TEAM ?> | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?>" />
<title>Vincaps Capital <?php echo _HEADER_TEAM ?> | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?></title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

 <div class="width100 overflow same-padding ow-about-us co-ow">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn" data-wow-delay="0.2s" >VinCaps Capital Sdn Bhd</p>
        <div class="line-startup ow-section-line float-right wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
 		<div class="text-center three-counter">
        	<img src="img/raise-fund.png" class="about-png wow fadeIn" data-wow-delay="0.6s" alt="VinCaps <?php echo _TEAM_STARTUP_FUNDING ?>" title="VinCaps <?php echo _TEAM_STARTUP_FUNDING ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s">VinCaps <?php echo _TEAM_STARTUP_FUNDING ?></p>
            
        </div>
 		<div class="text-center three-counter">
        	<img src="img/target.png" class="about-png wow fadeIn" data-wow-delay="1s" alt="VinCaps <?php echo _TEAM_CORPO ?>" title="VinCaps <?php echo _TEAM_CORPO ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.2s">VinCaps <?php echo _TEAM_CORPO ?></p>

        </div>        
  		<div class="text-center three-counter">
        	<img src="img/shake-hands.png" class="about-png wow fadeIn" data-wow-delay="1.4s" alt="VinCaps <?php echo _TEAM_CORPO_ADV ?>" title="VinCaps <?php echo _TEAM_CORPO_ADV ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.6s">VinCaps <?php echo _TEAM_CORPO_ADV ?></p>
            
        </div>    
  		<div class="text-center three-counter">
        	<img src="img/job.png" class="about-png wow fadeIn" data-wow-delay="1.8s" alt="VinCaps <?php echo _TEAM_CORPO_GOV ?>" title="VinCaps <?php echo _TEAM_CORPO_GOV ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2s">VinCaps <?php echo _TEAM_CORPO_GOV ?></p>
            
        </div>           
  		<div class="text-center three-counter">
        	<img src="img/private-equity.png" class="about-png wow fadeIn" data-wow-delay="2.2s" alt="VinCaps <?php echo _TEAM_CORPO_INV ?>" title="VinCaps <?php echo _TEAM_CORPO_INV ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2.4s">VinCaps <?php echo _TEAM_CORPO_INV ?></p>
            
        </div>           
        
        
           
        
 </div>       
   <div class="clear"></div> 
 
 <div class="width100 overflow same-padding ow-about-us co-ow team-ow">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn" data-wow-delay="0.2s" >VinCaps <?php echo _TEAM_INTERNAL_TEAM ?></p>
        <div class="line-startup ow-section-line float-right wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
 		<div class="text-center four-div-css">
        	<img src="img/prof.png" class="about-png wow fadeIn" data-wow-delay="0.6s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _TEAM_MANAGING_PARTNER ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="1s">Kevin Yam</p>
        </div>
 		<div class="text-center four-div-css">
        	<img src="img/vincaps-blog.png" class="about-png wow fadeIn" data-wow-delay="1.2s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.4s"><?php echo _TEAM_VP_CORP ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="1.6s">Vivian Goh</p>
        </div> 		
 		<div class="text-center four-div-css">
        	<img src="img/government-grants.png" class="about-png wow fadeIn" data-wow-delay="1.8s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2s"><?php echo _TEAM_VP_INV ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="2.2s">Lee Yong Sheng</p>
        </div> 	        
 		<div class="text-center four-div-css">
        	<img src="img/cto.png" class="about-png wow fadeIn" data-wow-delay="2.4s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2.6s"><?php echo _TEAM_CTO ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="2.8s">Sze Jun King</p>
        </div> 	           
<!-- 		<div class="text-center four-div-css">
        	<img src="img/goal.png" class="about-png wow fadeIn" data-wow-delay="3s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="3.2s"><?php echo _TEAM_SENIOR_MARKETING ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="3.4s">Muhammad Fuad Hj. Rosli</p>
        </div> -->       
 		<div class="text-center four-div-css">
        	<img src="img/creative.png" class="about-png wow fadeIn" data-wow-delay="3s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="3.2s"><?php echo _TEAM_SENIOR_CREATIVE ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="3.4s">Sherry Tan</p>
        </div>            
 		<div class="text-center four-div-css">
        	<img src="img/insight.png" class="about-png wow fadeIn" data-wow-delay="3.6s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="3.8s"><?php echo _TEAM_MARKETING_EXE ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="4s">Joanne Ho</p>
        </div>        
<!-- 		<div class="text-center four-div-css">
        	<img src="img/equity-crowdfunding.png" class="about-png wow fadeIn" data-wow-delay="4.8s">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="5s"><?php echo _TEAM_FINANCE ?></p>
            <p class="count-text light-blue-text wow fadeIn" data-wow-delay="5.2s">Teoh Ewe Yang</p>
        </div> -->        
         
 </div>       
    <div class="clear"></div> 

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
       
    	<p class="icon-p1 text-center"><img src="img/call.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_CONTACT_US ?></h1>    
    	<div class="clear"></div>      
        
<div class="five-col float-left two-column-css">
  <form class="form-class extra-margin" action="utilities/submitContactUsFunction.php" method="POST">
    <input type="text" name="name" placeholder="<?php echo _INDEX_NAME ?>" class="input-name clean dark-blue-text wow fadeIn" data-wow-delay="0.6s" required >
    <input type="email" name="email" placeholder="<?php echo _INDEX_EMAIL ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="0.8s" required >
    <input type="text" name="telephone" placeholder="<?php echo _INDEX_CONTACT_NUMBER ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.0s" required >
    
    <select  class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.2s" id="selection" name="selection" required>
      <option class=" clean lato dark-blue-text" value='Investor'><?php echo _INDEX2_INVESTOR ?></option>
      <option class=" clean lato dark-blue-text" value='Business Owner'><?php echo _INDEX2_B_OWNER ?></option>
    </select> 

    <textarea name="comments" placeholder="<?php echo _INDEX_MESSAGE ?>" class="input-name input-message clean lato dark-blue-text wow fadeIn" data-wow-delay="1.4s" ></textarea>
    <div class="clear"></div>
    <table class="form-table">
      <tbody>
        <tr class="wow fadeIn" data-wow-delay="1.6s">
          <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_UPDATED_WITH ?></p></td>
        </tr>
        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
          <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_I_JUST_WANT ?></p></td>
        </tr>
      </tbody>
    </table>
    <div class="res-div">
      <input type="submit" name="send_email_button" value="<?php echo _INDEX_SEND ?>" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="1.8s">
    </div>
  </form>
</div>	

        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2s">
                <?php echo _INDEX_COMPANY ?><br>
                <b class="dark-blue-text weight900">Vincaps Capital Sdn. Bhd.</b>
        	</p>         
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.2s">
                <?php echo _INDEX_CONTACT_NUMBER ?><br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60163324691">+6016 332 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.4s">
                <?php echo _INDEX_EMAIL ?><br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.6s">
                <?php echo _INDEX_ADDRESS ?><br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.8s">
                <?php echo _INDEX_COMPANY_NO ?><br>
                <b class="dark-blue-text weight900">202001034287(1390608-M)</b>
        	</p>                
            
        <p class="contact-p lighter-blue-text">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3s" alt="Vincaps Whatsapp" title="Vincaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.2s" alt="Vincaps Facebook" title="Vincaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.4s" alt="Vincaps Instagram" title="Vincaps Instagram"></a>
             <a href="https://my.linkedin.com/company/vincaps-capital-sdn-bhd" target="_blank"><img src="img/linkedin.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.6s" alt="Vincaps LinkedIn" title="Vincaps LinkedIn"></a>
        </p>  
                      
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn ani42" data-wow-delay="3.6s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>    
    
  <div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>