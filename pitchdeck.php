<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/pitchdeck.com" />
<link rel="canonical" href="https://vincaps.com/pitchdeck.com" />
<meta property="og:title" content="Vincaps Capital | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?>" />
<title>Vincaps Capital | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?></title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 pitch-bg">
		<div class="same-padding gradient-bg1 wow fadeIn" data-wow-delay="0.3s">
        	<h1 class="white-text pitch-h1"><?php echo _PITCH_TITLE ?></h1>
        </div>	

</div>
<div class="clear"></div>
 <div class="width100 overflow same-padding ow-about-us">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn" data-wow-delay="0.3s" ><?php echo _PITCH_IMPORTANCE ?></p>
        <div class="line-startup ow-section-line float-right wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
 		<div class="text-center three-counter">
        	<img src="img/impression.png" class="about-png wow fadeIn" data-wow-delay="0.6s" >
            
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _PITCH_P1 ?></p>
        </div>
 		<div class="text-center three-counter">
        	<img src="img/idea.png" class="about-png wow fadeIn" data-wow-delay="1s" >
           
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="1.2s"><?php echo _PITCH_P2 ?></p>
        </div>        
  		<div class="text-center three-counter">
        	<img src="img/shake-hands.png" class="about-png wow fadeIn" data-wow-delay="1.4s">
            
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="1.6s"><?php echo _PITCH_P3 ?></p>
        </div>       
        
 </div>       
   <div class="clear"></div> 
 <div class="width100 overflow same-padding ow-about-us">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn" data-wow-delay="0.3s" ><?php echo _PITCH_RAISE ?></p>
        <div class="line-startup ow-section-line float-right wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
 		<div class="text-center three-counter">
        	<img src="img/good-impression.png" class="about-png wow fadeIn" data-wow-delay="0.6s" >
            
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _PITCH_P4 ?></p>
        </div>
 		<div class="text-center three-counter">
        	<img src="img/successful.png" class="about-png wow fadeIn" data-wow-delay="1s" >
           
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="1.2s"><?php echo _PITCH_P5 ?></p>
        </div>        
  		<div class="text-center three-counter">
        	<img src="img/documents.png" class="about-png wow fadeIn" data-wow-delay="1.4s">
            
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="1.6s"><?php echo _PITCH_P6 ?></p>
        </div>       
        <div class="clear"></div>
        <div class="width100 text-center overflow">
        	<a href="#contactus"><div class="input-submit blue-button white-text clean pointer lato wow fadeIn middle-button-size" data-wow-delay="1.9s"><?php echo _PITCH_GET_IN_TOUCH ?></div></a>
        </div>
 </div>       
   <div class="clear"></div> 
 <div class="width100 overflow same-padding ow-about-us">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ow-long-left" data-wow-delay="0.3s" ><?php echo _PITCH_COLLECTION ?></p>
        <div class="line-startup ow-section-line float-right ow-blue-line ow-short-line wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Airbnb-Pitch-Deck.pdf"  target="_blank"><img src="img/logo1.png" class="pitch-logo"></a>
        </div>
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Buffer-Pitch-Deck.pdf"  target="_blank"><img src="img/logo2.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/BuzzFeed-Pitch-Deck.pdf"  target="_blank"><img src="img/logo3.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Canva-Pitch-Deck.pdf"  target="_blank"><img src="img/logo4.png" class="pitch-logo"></a>
        </div>   
        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Coinbase-Pitch-Deck.pdf"  target="_blank"><img src="img/logo5.png" class="pitch-logo"></a>
        </div>
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Facebook-Pitch-Deck.pdf"  target="_blank"><img src="img/logo6.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Foursquare-Pitch-Deck.pdf"  target="_blank"><img src="img/logo7.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Grab-Pitch-Deck.pdf"  target="_blank"><img src="img/logo8.png" class="pitch-logo"></a>
        </div>         
        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Igotopia-Pitch-Deck.pdf"  target="_blank"><img src="img/logo9.png" class="pitch-logo"></a>
        </div>
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Kakitangan-Pitch-Deck.pdf"  target="_blank"><img src="img/logo10.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/LinkedIn-Pitch-Deck.pdf"  target="_blank"><img src="img/logo11.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Peloton-Pitch-Deck.pdf"  target="_blank"><img src="img/logo12.png" class="pitch-logo"></a>
        </div> 
        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/SEOMoz-Pitch-Deck.pdf"  target="_blank"><img src="img/logo13.png" class="pitch-logo"></a>
        </div>
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/SignatureMarket-Pitch-Deck.pdf"  target="_blank"><img src="img/logo14.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Snapchat-Pitch-Deck.pdf"  target="_blank"><img src="img/logo15.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/TikTok-Pitch-Deck.pdf"  target="_blank"><img src="img/logo16.png" class="pitch-logo"></a>
        </div>         
        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Tinder-Pitch-Deck.pdf"  target="_blank"><img src="img/logo17.png" class="pitch-logo"></a>
        </div>
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/Uber-Pitch-Deck.pdf"  target="_blank"><img src="img/logo18.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/WeWork-Pitch-Deck.pdf"  target="_blank"><img src="img/logo19.png" class="pitch-logo"></a>
        </div>        
		<div class="text-center four-div-css wow fadeIn" data-wow-delay="0.6s">
        	<a href="./pdf/YouTube-Pitch-Deck.pdf"  target="_blank"><img src="img/logo20.png" class="pitch-logo"></a>
        </div>   
                             
        <div class="clear"></div>
        <div class="width100 text-center overflow">
        	<a href="https://drive.google.com/drive/folders/1jsYCXAybAqgRkuVvMg7W2iQhjcrHt1mS?usp=sharing" target="_blank"><div class="input-submit blue-button white-text clean pointer lato wow fadeIn middle-button-size" data-wow-delay="0.8s"><?php echo _PITCH_BROWSE_ALL ?></div></a>
        </div>
 </div>       
   <div class="clear"></div> 

 <div class="width100 overflow same-padding light-blue-bg relative">
         
        <p class="blue-text lato top-des float-left ow-section-des timeline-ow wow fadeIn ow-long-left" data-wow-delay="0.3s" ><?php echo _PITCH_GET_PROVEN ?></p>
        <div class="line-startup ow-section-line float-right timeline-line wow fadeIn ow-short-line" data-wow-delay="0.6s"></div>
        <div class="clear"></div> 
        <div class=" follow-harm-width margin-top50 pitch-width">
            <div class="timeline-item wow fadeIn ow-date-size" data-wow-delay="0.9s" date-is='<?php echo _PITCH_ROCK_YOUR ?>'>
                
                <p class="top-p1"><?php echo _PITCH_ROCK_YOUR_P ?></p>
            </div>
            
            <div class="timeline-item wow fadeIn ow-date-size" data-wow-delay="1.2s" date-is='<?php echo _PITCH_CONFI ?>'>
                
                <p><?php echo _PITCH_CONFI_P ?></p>
            </div>
            <div class="timeline-item wow fadeIn ow-date-size" data-wow-delay="1.5s" date-is='<?php echo _PITCH_BE_PREPARED ?>'>
                
                <p class="top-p3"><?php echo _PITCH_BE_PREPARED_P ?></p>
            </div>	
            <div class="timeline-item wow fadeIn ow-date-size" data-wow-delay="1.8s" date-is='<?php echo _PITCH_DUNNO ?>'>
                
                <p class="top-p4"><?php echo _PITCH_DUNNO_P ?></p>
            </div>     
          
            
        </div>        
         <div class="ripple-background wow fadeIn" data-wow-delay="2.1s" >
          <div class="circle-ri xxlarge shade1"></div>
          <div class="circle-ri xlarge shade2"></div>
          <div class="circle-ri large shade3"></div>
          <div class="circle-ri mediun shade4"></div>
          <div class="circle-ri small shade5"></div>
        </div> 
 </div>
    <div class="clear"></div> 

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
       
    	<p class="icon-p1 text-center"><img src="img/call.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_CONTACT_US ?></h1>    
    	<div class="clear"></div>      
        
<div class="five-col float-left two-column-css">
  <form class="form-class extra-margin" action="utilities/submitContactUsFunction.php" method="POST">
    <input type="text" name="name" placeholder="<?php echo _INDEX_NAME ?>" class="input-name clean dark-blue-text wow fadeIn" data-wow-delay="0.6s" required >
    <input type="email" name="email" placeholder="<?php echo _INDEX_EMAIL ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="0.8s" required >
    <input type="text" name="telephone" placeholder="<?php echo _INDEX_CONTACT_NUMBER ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.0s" required >
    
    <select  class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.2s" id="selection" name="selection" required>
      <option class=" clean lato dark-blue-text" value='Investor'><?php echo _INDEX2_INVESTOR ?></option>
      <option class=" clean lato dark-blue-text" value='Business Owner'><?php echo _INDEX2_B_OWNER ?></option>
    </select> 

    <textarea name="comments" placeholder="<?php echo _INDEX_MESSAGE ?>" class="input-name input-message clean lato dark-blue-text wow fadeIn" data-wow-delay="1.4s" ></textarea>
    <div class="clear"></div>
    <table class="form-table">
      <tbody>
        <tr class="wow fadeIn" data-wow-delay="1.6s">
          <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_UPDATED_WITH ?></p></td>
        </tr>
        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
          <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_I_JUST_WANT ?></p></td>
        </tr>
      </tbody>
    </table>
    <div class="res-div">
      <input type="submit" name="send_email_button" value="<?php echo _INDEX_SEND ?>" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="1.8s">
    </div>
  </form>
</div>	

        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2s">
                <?php echo _INDEX_COMPANY ?><br>
                <b class="dark-blue-text weight900">Vincaps Capital Sdn. Bhd.</b>
        	</p>         
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.2s">
                <?php echo _INDEX_CONTACT_NUMBER ?><br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60163324691">+6016 332 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.4s">
                <?php echo _INDEX_EMAIL ?><br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.6s">
                <?php echo _INDEX_ADDRESS ?><br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.8s">
                <?php echo _INDEX_COMPANY_NO ?><br>
                <b class="dark-blue-text weight900">202001034287(1390608-M)</b>
        	</p>                
            
        <p class="contact-p lighter-blue-text">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3s" alt="Vincaps Whatsapp" title="Vincaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.2s" alt="Vincaps Facebook" title="Vincaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.4s" alt="Vincaps Instagram" title="Vincaps Instagram"></a>
             <a href="https://my.linkedin.com/company/vincaps-capital-sdn-bhd" target="_blank"><img src="img/linkedin.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.6s" alt="Vincaps LinkedIn" title="Vincaps LinkedIn"></a>
        </p>  
                  
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn ani42" data-wow-delay="3.6s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>    
    
  <div class="clear"></div>

<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>