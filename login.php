<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/login.php" />
<link rel="canonical" href="https://vincaps.com/login.php" />
<meta property="og:title" content="VinCaps | Your Startup Funding Consultant" />
<title>VinCaps | Your Startup Funding Consultant</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="two-bg-container overflow">
        <div class="top-building-div width100"></div>
        <div class="bottom-water-div width100"></div>
	</div>
    <div class="big-container-css width100 ow-big-container-css">
      <div class="blue-div-opa width100 small-padding">
      	<!--<span class="close-css inner-close  upper-close">&times;</span>-->
      	<img src="img/hi.png" class="hello-icon blue-icon2" alt="Login" title="Login">
      	<h1 class="white-text welcome lato welcome2">Login</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     <form  method="POST"  action="utilities/loginFunction.php">
    
            <div class="grey-border extra-spacing-bottom"></div>
    
            <p class="input-top-p">Email</p>
            <input class="input-name clean lato blue-text" type="email" placeholder="Email" id="email" name="email" required>
    
            <p class="input-top-p">Password</p>
            <div class="fake-input-div overflow ow-no-margin-bottom">
            	<input class="input-name clean password-input lato blue-text" type="password" placeholder="Password" id="password" name="password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
    		</div>
    		<!--<a class="open-forgot forgot-a blue-link lato">Forgot Password?</a>-->
            <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="login">Login</button>
    		<!--<p class="signup-p text-center"><a class="open-signup blue-link lato text-center signup-a">No yet have an account? Sign up now!</a></p>-->
           
    
          </form>


    </div>
  </div>

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}

</style>







<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>