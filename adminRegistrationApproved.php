<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Registration.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

// $pendingMember = getRegistration($conn);
$pendingMember = getRegistration($conn, "WHERE remark_one = 'Approved' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/adminRegistrationApproved.php" />
<link rel="canonical" href="https://vincaps.com/adminRegistrationApproved.php" />
<meta property="og:title" content="VinCaps | Claimed Vourcher" />
<title>VinCaps | Claimed Vourcher</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

    <h1 class="price-h1 dark-blue-text lato"><a href="adminRegistrationWebinar.php" class="light-blue-link">Webinar Registration</a> | <a href="adminRegistrationPending.php" class="light-blue-link">Unclaimed Voucher</a> | Claimed Vourcher</h1>
    
	<div class="clear"></div>

    <div class="big-four-input-container">
        <div class="width100">
            <p class="input-top-p">Search Voucher Code</p>
            <input class="input-name clean lato blue-text" type="text" onkeyup="myFunction()" placeholder="Voucher Code" id="myInput" name="myInput">
        </div>
    </div>

	<div class="clear"></div>

    <div class="scroll-div margin-top30">
  
        <!-- <table class="approve-table lato"> -->
        <table class="approve-table lato" id="myTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Fullname</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Voucher Code</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($pendingMember)
                    {
                        for($cnt = 0;$cnt < count($pendingMember) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $pendingMember[$cnt]->getFullname();?></td>
                                <td><?php echo $pendingMember[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $pendingMember[$cnt]->getEmail();?></td>
                                <td><?php echo $pendingMember[$cnt]->getUid();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

</div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Voucher Verified !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !!","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>