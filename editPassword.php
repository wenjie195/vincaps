<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/editPassword.php" />
<link rel="canonical" href="https://vincaps.com/editPassword.php" />
<meta property="og:title" content="VinCaps | Edit Password" />
<title>VinCaps | Edit Password</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="two-bg-container overflow">
        <div class="top-building-div width100"></div>
        <div class="bottom-water-div width100"></div>
	</div>
    <div class="big-container-css width100">
        <div class="blue-div-opa width100 small-padding">
            <!--<span class="close-css inner-close  upper-close">&times;</span>-->
            <img src="img/password.png" class="hello-icon blue-icon2" alt="Edit Password" title="Edit Password">
            <h1 class="white-text welcome lato welcome2">Edit Password</h1>
        </div>
        <div class="white-bg width100 small-padding overflow below-blue-box">
            <!-- <form action="" method="POST" > -->
            <form method="POST" action="utilities/editPasswordFunction.php">
                <p class="input-top-p">Current Password</p>
                <div class="fake-input-div overflow">
                <input class="input-name clean password-input lato blue-text" type="password" placeholder="Password" id="password" name="password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>

                <p class="input-top-p">New Password</p>
                <div class="fake-input-div overflow">
                <input class="input-name clean password-input lato blue-text" type="password" placeholder="New Password" id="register_password" name="register_password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div> 

                <p class="input-top-p">Retype New Password</p>
                <div class="fake-input-div overflow">
                <input class="input-name clean password-input lato blue-text" type="password" placeholder="Retype New Password" id="register_retype_password" name="register_retype_password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
                </div>             
                <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="submit">Submit</button>
            </form>
        </div>
    </div>
<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>
<?php include 'js.php'; ?>

</body>
</html>