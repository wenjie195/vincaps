<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

// $articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");
$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/fundraisingBlog.php" />
<link rel="canonical" href="https://vincaps.com/fundraisingBlog.php" />
<meta property="og:title" content="Vincaps Capital | <?php echo _INDEX_BLOG ?>" />
<title>Vincaps Capital | <?php echo _INDEX_BLOG ?></title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">
<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'header.php'; ?>
<div class="background-div">
    <div class="cover-gap content index-content min-height2 overflow">

        <div class="test overflow">

    <h1 class="light-blue-text lato border-text op padding20"><b>Vincaps <?php echo _INDEX_BLOG ?></b></h1>

	<div class="big-white-div padding20" id="app">
        <?php
        $conn = connDB();
        if($articles)
        {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
        <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
            <div class="article-card article-card-overwrite">
            <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="overwrite-img-a"></a>
            <div class="article-bg-img-box progressive">
            	
  					<img data-src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" src="img/preload.jpg" class="width100 preview lazy" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>"/>
				</a>
           </div>           
           <div class="box-caption box2">
            <div class="wrap-a wrap100">
            <span class="light-blue-text small-date"><a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="light-blue-text small-date"><?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?></span>
            </div>
 <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a dark-blue-text text-overflow">
                                    <?php echo $articles[$cnt]->getTitle();?>
                                </div>

                                <div class="text-content-div dark-blue-text">
                                    <?php echo $description = $articles[$cnt]->getKeywordOne();?>

                                </div>
								</a>
                            </div>
                            
                        </div>
                    </a>            
            
            
            

        <?php
        }
        ?>
        <?php
        }
        $conn->close();
        ?>
	</div>

</div>
</div>
<?php include 'js.php'; ?>

</body>
</html>