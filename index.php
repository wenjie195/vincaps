<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");
$announcement = getAnnouncement($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="Vincaps Capital | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?>" />
<title>Vincaps Capital | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?></title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 overflow top1-div same-padding">
	<img src="img/vincaps-logo.png" class="top-logo" alt="Vincaps Capital" title="Vincaps Capital">
	<p class="top-banner-p lato wow fadeIn" data-wow-delay="0.2s"><?php echo _INDEX_TOP_P ?></p>
    <a href="#contactus"><div class="orange-button hover-effect white-text lato wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX_JOIN ?></div></a>
</div>

    <div class="clear"></div> 

    <div class="width100 overflow second-div1 same-padding" id="aboutus">  
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s"><?php echo _HEADER_ABOUT_US ?></h1>
            <p class="about-p2 p-size black-text wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX2_ABOUT ?></p>
                    <div class="clear"></div>

 		<div class="text-center three-counter">
        	<img src="img/target.png" class="about-png wow fadeIn" data-wow-delay="0.6s" alt="<?php echo _INDEX_VISION ?>" title="<?php echo _INDEX_VISION ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX_VISION ?></p>
            <p class="p-size black-text wow fadeIn" data-wow-delay="1s"><?php echo _INDEX_TO_CREATE_AN ?></p>
        </div>        
  		<div class="text-center three-counter">
        	<img src="img/goal.png" class="about-png wow fadeIn" data-wow-delay="1.2s" alt="<?php echo _INDEX_MISSION ?>" title="<?php echo _INDEX_MISSION ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.4s"><?php echo _INDEX_MISSION ?></p>
            <p class="p-size black-text wow fadeIn" data-wow-delay="1.6s"><?php echo _INDEX_TO_MAX ?></p>
        </div> 
  		<div class="text-center three-counter">
        	<img src="img/slogan.png" class="about-png wow fadeIn" data-wow-delay="1.8s" alt="<?php echo _INDEX_MOTTO ?>" title="<?php echo _INDEX_MOTTO ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2s"><?php echo _INDEX_MOTTO ?></p>
            <p class="p-size black-text wow fadeIn" data-wow-delay="2.2s"><?php echo _INDEX_ONCE_READY ?></p>
        </div>           
    </div>


    <div class="same-padding width100 white-spacing-div overflow" id="app">

        <?php
        $conn = connDB();
        if($announcement)
        {
        ?>

            <p class="icon-p1 text-center"><img src="img/speaker.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
            <h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX_IN_THE_NEWS ?></h1>

            <div class="clear"></div>  

        <?php
        }
        $conn->close();
        ?>

        <?php
        $conn = connDB();
        if($announcement)
        {
        for($cnt = 0;$cnt < count($announcement) ;$cnt++)
        {
        ?>
            <a href='announcement.php?id=<?php echo $announcement[$cnt]->getUid();?>' class="announce-a shadow-hover announcement-card article-card article-card-overwrite  wow fadeIn ow-mh0 " data-wow-delay="0.6s">
                <div class="">
                    <div class="square progressive square-news">
                    <div class="content2">
                        <img data-src="uploadsArticle/<?php echo $announcement[$cnt]->getTitleCover();?>" src="img/preload.jpg" class="preview width100 lazy" alt="<?php echo $announcement[$cnt]->getTitle();?>" title="<?php echo $announcement[$cnt]->getTitle();?>"/>
                    
                    </div>  
					</div>  
                    <div class="box-caption box2 box2-right">
                        <div class="wrap-a wrap100">
                            <span class="light-blue-text small-date"><?php echo $date = date("d-m-Y",strtotime($announcement[$cnt]->getDateCreated()));?></span>
                        </div>

                       
                            <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a dark-blue-text text-overflow">
                                <?php echo $announcement[$cnt]->getTitle();?>
                            </div>
                            <div class="text-content-div text-content-div-mh dark-blue-text">
                                <?php echo $announcement[$cnt]->getKeywordOne();?>
                            </div>
                      
                    </div>
                </div>
            </a>    
        <?php
        }
        ?>
        <?php
        }
        $conn->close();
        ?>

        <?php
        $conn = connDB();
        if($announcement)
        {
        ?>

            <div class="width100 overflow text-center">
                <a href="allAnnouncement.php"><div class="orange-button2 hover-effect white-text lato wow fadeIn"  data-wow-delay="0.8s"><?php echo _INDEX2_READ_MORE ?></div></a>
            </div>

        <?php
        }
        $conn->close();
        ?>


    </div>  


    <div class="clear"></div>
 
    <div class="width100 overflow second-div1 same-padding" id="journey"> 
    		<p class="icon-p1 text-center"><img src="img/sail.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_OUR_JOURNEY ?></h1>
			<div class="clear"></div>  		
    		<div class="left-vector-div wow bounceInLeft" data-wow-delay="0.6s">
            	<img src="img/pic4.png" class="left-vector-png">
            </div>
            <div class="right-text-div wow bounceInRight" data-wow-delay="0.8s">
            	<p class="p-size black-text"><?php echo _INDEX2_JOURNEY1 ?></p>
            </div>
            <div class="clear"></div>
           <p class="arrow-img-p"><img src="img/dropdown3.png" class="black-drop wow fadeIn" data-wow-delay="1s"></p>
			<div class="clear"></div>  		
    		<div class="left-vector-div wow bounceInLeft" data-wow-delay="1.2s">
            	<img src="img/pic5.png" class="left-vector-png">
            </div>
            <div class="right-text-div wow bounceInRight" data-wow-delay="1.4s">
            	<p class="p-size black-text"><?php echo _INDEX2_JOURNEY2 ?></p>
            </div>
            <div class="clear"></div>
           <p class="arrow-img-p"><img src="img/dropdown3.png" class="black-drop wow fadeIn" data-wow-delay="1.6s"></p>           
            <div class="clear"></div>
          
    		<div class="left-vector-div wow bounceInLeft" data-wow-delay="1.8s">
            	<img src="img/pic6.png" class="left-vector-png">
            </div>
            <div class="right-text-div wow bounceInRight" data-wow-delay="2s">
            	<p class="p-size black-text"><?php echo _INDEX2_JOURNEY3 ?></p>
            </div>
            <div class="clear"></div>           
           
           
    </div>
    <div class="clear"></div>
 
    <div class="width100 overflow second-div1 same-padding" id="performance"> 
    		<p class="icon-p1 text-center"><img src="img/target2.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_OUR_PERFORMANCE ?></h1>
			<div class="clear"></div>   
            <div class="text-center two-box-div">
                <img src="img/private-equity.png" class="about-png wow fadeIn" data-wow-delay="0.6s">
                
                <p class="content-text-p black-text wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX2_PERFORMANCE1 ?></p>
            </div>
            <div class="text-center two-box-div">
                <img src="img/successful.png" class="about-png wow fadeIn" data-wow-delay="1s">
                
                <p class="content-text-p black-text wow fadeIn" data-wow-delay="1.2s"><?php echo _INDEX2_PERFORMANCE2 ?></p>
            </div>      
  
   </div>
 <div class="clear"></div> 
    <div class="width100 overflow second-div1 same-padding" id="services"> 
    		<p class="icon-p1 text-center"><img src="img/work.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_OUR_SERVICES ?></h1>
			<div class="clear"></div>  		
    		<div class="left-vector-div wow bounceInLeft" data-wow-delay="0.6s">
            	<img src="img/pic7.png" class="left-vector-png">
            </div>
            <div class="right-text-div wow bounceInRight" data-wow-delay="0.8s">
            	<p class="tittle-text-p dark-blue-text"><?php echo _INDEX2_SERVICES1 ?></p>
            	<p class="p-size black-text"><?php echo _INDEX2_SERVICES_P1 ?></p>
            </div>
            <div class="clear"></div>
           
    		<div class="left-vector-div wow bounceInLeft" data-wow-delay="1s">
            	<img src="img/pic8.png" class="left-vector-png">
            </div>
            <div class="right-text-div wow bounceInRight" data-wow-delay="1.2s">
           		 <p class="tittle-text-p dark-blue-text"><?php echo _INDEX2_SERVICES2 ?></p>
            	<p class="p-size black-text"><?php echo _INDEX2_SERVICES_P2 ?></p>
            </div>
                      
            <div class="clear"></div>
          
    		<div class="left-vector-div wow bounceInLeft" data-wow-delay="1.4s">
            	<img src="img/pic9.png" class="left-vector-png">
            </div>
            <div class="right-text-div wow bounceInRight" data-wow-delay="1.6s">
            	<p class="tittle-text-p dark-blue-text"><?php echo _INDEX2_SERVICES3 ?></p>
            	<p class="p-size black-text"><?php echo _INDEX2_SERVICES_P3 ?></p>
            </div>
            <div class="clear"></div>            
    </div>
  <div class="clear"></div> 
    <div class="width100 overflow second-div1 same-padding" id="investment">  
    	<p class="icon-p1 text-center"><img src="img/magnify.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_FOCUS_AREA ?></h1>
            <p class="about-p2 p-size black-text wow fadeIn" data-wow-delay="0.6s"><?php echo _INDEX2_FOCUS_AREA_DESC ?></p>
                    <div class="clear"></div>    
     		<div class="text-center four-div-css">
                <img src="img/equity-crowdfunding.png" class="about-png wow fadeIn" data-wow-delay="0.6s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX2_ECOMMERCE ?></p>
        	</div>
     		<div class="text-center four-div-css">
                <img src="img/iot.png" class="about-png wow fadeIn" data-wow-delay="1s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.2s"><?php echo _INDEX2_IOT ?></p>
        	</div>            
     		<div class="text-center four-div-css">
                <img src="img/cto.png" class="about-png wow fadeIn" data-wow-delay="1.4s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.6s"><?php echo _INDEX2_ESSENTIAL ?></p>
        	</div>                
     		<div class="text-center four-div-css">
                <img src="img/game.png" class="about-png wow fadeIn" data-wow-delay="1.8s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2s"><?php echo _INDEX2_GAMING ?></p>
        	</div>  
            <div class="clear"></div>
            <p class="p-size light-blue-text text-center wow fadeIn" data-wow-delay="2.2s"><?php echo _INDEX2_IF_YOUR ?></p>          
    </div>
  <div class="clear"></div> 
    <div class="width100 overflow second-div1 same-padding" id="investment">  
    	<p class="icon-p1 text-center"><img src="img/money.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX2_FEE ?></h1>    
    	<div class="clear"></div>
        <div class="fee-div wow fadeIn" data-wow-delay="0.6s">
        	<div class="dark-blue-bg top-fee"><p class="tittle-text-p white-text"><?php echo _INDEX2_FEE_TITLE ?></p></div>
            <div class="bottom-fee">
            	<p class="p-size dark-blue-text fee-p"><?php echo _INDEX2_FEE1 ?></p>
                <p class="p-size dark-blue-text fee-p"><?php echo _INDEX2_FEE2 ?></p>
                <p class="p-size dark-blue-text fee-p"><?php echo _INDEX2_FEE3 ?></p>
                <p class="p-size dark-blue-text fee-p"><?php echo _INDEX2_FEE4 ?></p>
                <p class="p-size dark-blue-text fee-p"><?php echo _INDEX2_FEE5 ?></p>
                <p class="p-size dark-blue-text fee-p"><?php echo _INDEX2_FEE6 ?></p>
            </div>
        </div>
    	<div class="clear"></div>
    		<p class="p-size text-center wow fadeIn" data-wow-delay="0.8s"><a href="#terms" class="hover-effect blue-link2"><?php echo _INDEX2_TERMS_APPLIED ?></a></p>
    </div>
    <div class="clear"></div> 

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
       
    	<p class="icon-p1 text-center"><img src="img/call.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_CONTACT_US ?></h1>    
    	<div class="clear"></div>      
        
<div class="five-col float-left two-column-css">
  <form class="form-class extra-margin" action="utilities/submitContactUsFunction.php" method="POST">
    <input type="text" name="name" placeholder="<?php echo _INDEX_NAME ?>" class="input-name clean dark-blue-text wow fadeIn" data-wow-delay="0.6s" required >
    <input type="email" name="email" placeholder="<?php echo _INDEX_EMAIL ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="0.8s" required >
    <input type="text" name="telephone" placeholder="<?php echo _INDEX_CONTACT_NUMBER ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.0s" required >
    
    <select  class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.2s" id="selection" name="selection" required>
      <option class=" clean lato dark-blue-text" value='Investor'><?php echo _INDEX2_INVESTOR ?></option>
      <option class=" clean lato dark-blue-text" value='Business Owner'><?php echo _INDEX2_B_OWNER ?></option>
    </select> 

    <textarea name="comments" placeholder="<?php echo _INDEX_MESSAGE ?>" class="input-name input-message clean lato dark-blue-text wow fadeIn" data-wow-delay="1.4s" ></textarea>
    <div class="clear"></div>
    <table class="form-table">
      <tbody>
        <tr class="wow fadeIn" data-wow-delay="1.6s">
          <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_UPDATED_WITH ?></p></td>
        </tr>
        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
          <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_I_JUST_WANT ?></p></td>
        </tr>
      </tbody>
    </table>
    <div class="res-div">
      <input type="submit" name="send_email_button" value="<?php echo _INDEX_SEND ?>" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="1.8s">
    </div>
  </form>
</div>	

        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2s">
                <?php echo _INDEX_COMPANY ?><br>
                <b class="dark-blue-text weight900">Vincaps Capital Sdn. Bhd.</b>
        	</p>         
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.2s">
                <?php echo _INDEX_CONTACT_NUMBER ?><br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60163324691">+6016 332 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.4s">
                <?php echo _INDEX_EMAIL ?><br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.6s">
                <?php echo _INDEX_ADDRESS ?><br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.8s">
                <?php echo _INDEX_COMPANY_NO ?><br>
                <b class="dark-blue-text weight900">202001034287(1390608-M)</b>
        	</p>                
            
        <p class="contact-p lighter-blue-text">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3s" alt="Vincaps Whatsapp" title="Vincaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.2s" alt="Vincaps Facebook" title="Vincaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.4s" alt="Vincaps Instagram" title="Vincaps Instagram"></a>
             <a href="https://my.linkedin.com/company/vincaps-capital-sdn-bhd" target="_blank"><img src="img/linkedin.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="3.6s" alt="Vincaps LinkedIn" title="Vincaps LinkedIn"></a>
        </p>  
            <p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.8s">
                <a href="#terms" class="blue-link2 hover-effect"><?php echo _INDEX2_TERMS_AND ?></a>
        	</p>                        
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn" data-wow-delay="3.6s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>    
    
  <div class="clear"></div>
 
<div class="same-padding width100 white-spacing-div overflow" id="app">
    	<p class="icon-p1 text-center"><img src="img/blog.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX_BLOG ?></h1>    
    	<div class="clear"></div>  

        <?php
        $conn = connDB();
        if($articles)
        {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
        <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
            <div class="article-card article-card-overwrite opacity-hover wow fadeIn" data-wow-delay="0.6s">
           
            <div class="article-bg-img-box progressive">
            	
  					<img data-src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" src="img/preload.jpg" class="preview width100 lazy" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>"/>
				</a>
           </div>           
           <div class="box-caption box2">
            <div class="wrap-a wrap100">
            <span class="light-blue-text small-date"><a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="light-blue-text small-date"><?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?></span>
            </div>
 <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a dark-blue-text text-overflow">
                                    <?php echo $articles[$cnt]->getTitle();?>
                                </div>

                                <div class="text-content-div dark-blue-text">
                                    <?php echo $description = $articles[$cnt]->getKeywordOne();?>

                                </div>
								</a>
                            </div>
                            
                        </div>
                    </a>    
        <?php
        }
        ?>
        <?php
        }
        $conn->close();
        ?>
		<div class="width100 overflow text-center">
        	<a href="fundraisingBlog.php"><div class="orange-button2 hover-effect white-text lato wow fadeIn"  data-wow-delay="0.8s"><?php echo _INDEX2_READ_MORE ?></div></a>
        </div>

</div>  
<div class="clear"></div>   
<div class="width100 overflow terms-div same-padding" id="terms">
	<p class="terms-p">
    	<b>Terms & Conditions</b>
        <br><br>
        <b>OVERVIEW</b>
        <br>
        Kindly read & understand these Terms of Service ("Terms", "Terms of Service") carefully before using the vincaps.com website (the "Service") operated by Vincaps Capital Sdn Bhd as licensed by Vincaps Capital. ("Vincaps","us", "we", or "our"). 
		<br>
        Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms.
        <br>
        These Terms apply to all visitors, users and others who access or use the Service. You warrant that you are at least 18-years-old and you are legally capable of entering into binding contracts. If you are under 18-years-old, you warrant that you have obtained consent from your parent or guardian and they agree to be bound by these Terms on your behalf.
		<br>
        You warrant that you are either one of the following: a Malaysian citizen, Malaysian Permanent resident, Employment Pass holder or Work Permit holder.
        <br>
        Your access or use of the Service constitutes your agreement to be bound by these Terms. We reserve the right to modify or update these terms at any time without prior notice. If you disagree with any part of the terms outlined, then you may not access the Service.
        <br><br>
    	<b>COMMUNICATIONS</b>
        <br>
        Use of the Service or any form of electronic communication to Vincaps Capital including but not limited to emails sent to Vincaps Capital, conversations within the live chat application, will constitute your consent to receiving communications from Vincaps Capital.
        <br><br>
        <b>INTELLECTUAL PROPERTY, COPYRIGHTS AND TRADEMARKS</b>
        <br>
        The Service and its features, functionality and all its content is the property of Vincaps Capital or its content licensors and is protected by Malaysia and international copyright laws. Graphics, logos, scripts, icons made available in the Service are trademarks and trade dress of Vincaps Capital and may not be used in connection with any product or service without the prior written consent of Vincaps Capital. Use of the Service does not constitute a transfer of any Intellectual Property rights from us to you.
        <br><br>
        <b>SEVERABILITY</b>
        <br>
        We reserve the right, at our sole discretion, to modify or replace these Terms at any time. Your continued access to or use of the Service will be deemed as continued acceptance of the Terms.
        <br><br>
        <b>CONDITIONAL ASSISTANCE</b>
        <br>        
    	If you need additional information regarding the Terms, please contact us via: hello.vincaps@gmail.com

    
    </p>

</div>    
    
    
    
    
    
 

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>