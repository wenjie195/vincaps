<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/login.php" />
<link rel="canonical" href="https://vincaps.com/login.php" />
<meta property="og:title" content="VinCaps | Webinar Registration" />
<title>VinCaps | Webinar Registration</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="two-bg-container overflow">
    <div class="top-building-div width100"></div>
    <div class="bottom-water-div width100"></div>
</div>

<div class="big-container-css width100 ow-big-container-css">
    <div class="blue-div-opa width100 small-padding">
        <!-- <img src="img/hi.png" class="hello-icon blue-icon2" alt="Registration" title="Registration"> -->
        <h1 class="white-text welcome lato welcome2">Registration</h1>
    </div>

    <div class="white-bg width100 small-padding overflow below-blue-box">
        <form  method="POST" action="utilities/registrationFunction.php">
            <div class="grey-border extra-spacing-bottom"></div>

            <p class="input-top-p">Name</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Name" id="name" name="name" required>

            <p class="input-top-p">Email</p>
            <input class="input-name clean lato blue-text" type="email" placeholder="Email" id="email" name="email" required>

            <p class="input-top-p">Phone</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Phone" id="phone" name="phone" required>

            <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="submit">Submit</button>
        </form>
    </div>
</div>

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>

<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>