<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/Package.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/adminBlogView.php" />
<link rel="canonical" href="https://vincaps.com/adminBlogView.php" />
<meta property="og:title" content="VinCaps | Admin View Message" />
<title>VinCaps | Admin View Message</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height overflow">

    <h1 class="line-header">Contact Us Message</h1>
    
	<div class="clear"></div>

    <div class="">
  
        <?php
        if(isset($_POST['item_uid']))
        {
            $conn = connDB();
            $messageRows = getPackage($conn," WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
            // $messageDetails = $messageRows[0];
            {
            ?>
            <p class="p-size black-text">
            	Name: <b><?php echo $messageRows[0]->getUsername();?></b><br>
                Email: <b><?php echo $messageRows[0]->getEmail();?></b><br>
                Phone: <b><?php echo $messageRows[0]->getPhone();?></b><br>
                Status: <b><?php echo $messageRows[0]->getPackage();?></b><br>
                Subscribe to Newsletter: <b><?php echo $messageRows[0]->getNewsletter();?></b><br><br>
                Message:<br><?php echo $messageRows[0]->getMessage();?>
            </p>

            <?php
            }
            $conn->close();
        }
        ?>
		<div class="width100 overflow text-center">
        	<a href="adminContactUsView.php"><div class="orange-button2 hover-effect white-text lato">Go Back</div></a>
        </div>
    </div>  

</div>

<?php include 'js.php'; ?>

</body>
</html>