<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/lead.php" />
<link rel="canonical" href="https://vincaps.com/lead.php" />
<meta property="og:title" content="Lead Form | VinCaps" />
<title>Lead Form | VinCaps</title>
<meta property="og:description" content="We help you to maximize your potential for funding and at the same time assisting and supporting you along the way through our bespoke and specialized incubation courses and acceleration processes." />
<meta name="description" content="We help you to maximize your potential for funding and at the same time assisting and supporting you along the way through our bespoke and specialized incubation courses and acceleration processes." />
<meta name="keywords" content="teh tarik, vincaps, kevin, Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 overflow text-center teh2-div same-padding text-center padding-top-bottom2">
	<h1 class="teh-all-h1 dark-blue-text wow fadeIn" data-wow-delay="0.2s">With Us, You’ll Get</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    
    <div class="ow-no-question">
 		<div class="text-center three-column">
        	<img src="img/shake-hands.png" class=" wow fadeIn" data-wow-delay="0.6s" >
            <p class=" wow fadeIn" data-wow-delay="0.8s">Guidance to <b class="blue-text">approach investors</b> with the latest funding sources</p>
        </div>
 		<div class="text-center three-column mid-three-column second-three-column">
        	<img src="img/prof.png" class="wow fadeIn" data-wow-delay="1s" >
            <p class=" wow fadeIn" data-wow-delay="1.2s">Network to meet the <b class="blue-text">giant investors or PE</b></p>
        </div>        
  		<div class="text-center three-column">
        	<img src="img/idea.png" class="wow fadeIn" data-wow-delay="1.4s" >
            <p class=" wow fadeIn" data-wow-delay="1.6s">1 to 1 coaching to guide your <b class="blue-text">'pitching rehearsals for reality'</b></p>
        </div> 
  		<div class="text-center three-column">
        	<img src="img/design-picthdeck.png" class=" wow fadeIn" data-wow-delay="1.8s" >
            <p class=" wow fadeIn" data-wow-delay="2s">Get all your winning ingredients in front of the investors with our <b class="blue-text">“Proven” pitch deck structure</b></p>
        </div>
 		<div class="text-center three-column mid-three-column second-three-column">
        	<img src="img/10x.png" class="wow fadeIn" data-wow-delay="2.2s">
            <p class=" wow fadeIn" data-wow-delay="2.2s">Simple step approach to quickly screen, redesign your business plan and <b class="blue-text">reap 10x returns on your investment</b></p>
        </div>        
  		<div class="text-center three-column">
        	<img src="img/speed.png" class="wow fadeIn" data-wow-delay="2.4s" >
            <p class=" wow fadeIn" data-wow-delay="2.6s">An opportunity to <b class="blue-text">raise fund</b> with the <b class="blue-text">fastest route</b></p>
        </div>      
     </div>   
</div>
<div class="clear"></div>       

  

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani31" data-wow-delay="0.3s">Contact Us</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani31" data-wow-delay="0.5s"></div>
		<div class="clear"></div>       
        <div class="five-col float-left two-column-css">
   	<form id="contactform" method="post" action="index.php" class="form-class extra-margin">
        <input type="text" name="name" placeholder="Name" class="input-name clean dark-blue-text wow fadeIn ani32" data-wow-delay="0.8s" required >

                  <input type="email" name="email" placeholder="Email" class="input-name clean lato dark-blue-text wow fadeIn ani33" data-wow-delay="1.4s" required >

                  <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean lato dark-blue-text wow fadeIn ani34" data-wow-delay="2.0s" required >



                  <textarea name="comments" placeholder="Message" class="input-name input-message clean lato dark-blue-text wow fadeIn ani35" data-wow-delay="2.6s" ></textarea>
                  <div class="clear"></div>
                  <table class="form-table">
                  	<tbody>
                        <tr class="wow fadeIn ani36" data-wow-delay="3.2s">
                            <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
                            <td><p class="opt-msg lato dark-blue-text">I want to be updated with more information about your company's news and future promotions</p></td>
                        </tr>
                        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
                            <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
                            <td><p class="opt-msg lato dark-blue-text"> I just want to be contacted based on my request/inquiry</p></td>
                        </tr>
                    </tbody>
                  </table>

                  <div class="res-div"><input type="submit" name="send_email_button" value="Send" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="4.4s"></div>


        </form>
            
        </div>	
        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text tav wow fadeIn ani39" data-wow-delay="4.9s">
                Contact Number<br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60163324691">+6016 332 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn ani40" data-wow-delay="5.4s">
                Email Address<br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text tav wow fadeIn ani41" data-wow-delay="5.9s">
                Address<br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>            
            
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="6.4s" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="6.9s" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="7.4s" alt="VinCaps Instagram" title="VinCaps Instagram"></a>
        </p>  
                           
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn ani42" data-wow-delay="7.8s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>


<?php include 'js.php'; ?>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "hello.vincaps@gmail.com";
    $email_subject = "Contact Form via VinCaps website";

    function died($error)
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }


    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }



    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments'];
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;

	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }


    $string_exp = "/^[A-Za-z .'-]+$/";

  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }




  if(strlen($error_message) > 0) {
    died($error_message);
  }

    $email_message = "Form details below.\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }

    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
echo '<script>alert("Thank for messaging us. You can visit our Facebook page for more details while waiting for the reply.")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php

}
?>


</body>
</html>