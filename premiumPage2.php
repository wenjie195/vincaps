<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

// $userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="VinCaps | Your Startup Funding Consultant" />
<title>VinCaps | Your Startup Funding Consultant</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding">

    <?php
        $tz = 'Asia/Kuala_Lumpur';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $currentTime = $dt->format('Y-m-d');
        // echo $currentTime;
        // echo $currentTime = 'current time : 2020-11-09';
        // echo "<br>";

        $conn = connDB();
        $userRows = getUser($conn,"WHERE uid = ? ", array("uid") ,array($uid),"s");
        $expiredDate = $userRows[0]->getExpired();
        $status = $userRows[0]->getStatus();
        // echo $expiredDate = $userRows[0]->getExpired();
        // echo "<br>";

        if($status == 'Approved')
        {   
            if($expiredDate < $currentTime)
            // expired
            {   
            ?>
                <div class="dual-input">
                    <p class="input-top-p ow-black-text">your subscribtion already expired !! <br> Please renew</p>
                </div>
            <?php
            }
            else
            // premium user
            {   
            ?>
                <div class="dual-input">
                    <p class="input-top-p ow-black-text">enjoy the services !!</p>
                </div>
            <?php
            }
        }
        elseif($status == "")
        {   
        // not yet subscribe 
        ?>
            <div class="dual-input">
                <p class="input-top-p ow-black-text">not yet subscribe !!</p>
            </div>
        <?php
        }
        else
        {   
        // error checking
        ?>
            <div class="dual-input">
                <p class="input-top-p ow-black-text">error , please contact admin</p>
            </div>
        <?php
        }
        $conn->close();
    ?>

</div>

<?php include 'js.php'; ?>
</body>
</html>