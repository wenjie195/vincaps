<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Registration.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['registration_uid'];

$conn = connDB();

$userDetails = getRegistration($conn, " WHERE uid = ? ",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/ticketView.php" />
<link rel="canonical" href="https://vincaps.com/ticketView.php" />
<meta property="og:title" content="VinCaps | Registration Voucher" />
<title>VinCaps | Registration Voucher</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 overflow dark-blue-bg teh1-div same-padding">
	<div class="left-teh1">
    	<img src="img/milk.png" data-wow-delay="0.2s" class="milk wow fadeIn">
        <h1 class="white-text teh-h1 lato wow fadeIn" data-wow-delay="0.4s">Teh Tarik Session Voucher<img src="img/milktea.png" class="milktea wow fadeIn" data-wow-delay="0.6s" alt="Teh Tarik" title="Teh Tarik"></h1>
        <div class="teh-border white-border wow fadeIn" data-wow-delay="0.8s"></div>
        <p class="teh-content-p white-text wow fadeIn" data-wow-delay="1s">Thank you for registering.</p>
        <table class="teh-info-table lato">
        	<tr class=" wow fadeIn" data-wow-delay="1.2s">
            	<td>Date</td>
                <td>:</td>
                <td><b>2nd May (Sunday)</b></td>
            </tr>
        	<tr class=" wow fadeIn" data-wow-delay="1.4s">
            	<td>Time</td>
                <td>:</td>
                <td><b>9pm - 10pm</b></td>
            </tr> 
        	<tr class=" wow fadeIn" data-wow-delay="1.6s">
            	<td>Venue</td>
                <td>:</td>
                <td><b>Zoom and Facebook Live</b></td>
            </tr>   
        	<tr class=" wow fadeIn" data-wow-delay="1.8s">
            	<td>Your Name</td>
                <td>:</td>
                <td><b><?php echo $userData->getFullname();?></b></td>
            </tr>
        	<tr class=" wow fadeIn" data-wow-delay="2s">
            	<td>Your Email</td>
                <td>:</td>
                <td><b><?php echo $userData->getEmail();?></b></td>
            </tr> 
        	<tr class=" wow fadeIn" data-wow-delay="2.2s">
            	<td>Your Contact</td>
                <td>:</td>
                <td><b><?php echo $userData->getPhoneNo();?></b></td>
            </tr> 
        	<tr class=" wow fadeIn" data-wow-delay="2.4s">
            	<td>Voucher Code</td>
                <td>:</td>
                <td><b style="word-break:break-all;"><?php echo $userData->getUid();?></b></td>
            </tr>                                                
        </table>
    </div>
	<div class="right-teh1 wow fadeIn" data-wow-delay="2.6s">
    	<img src="img/teh-tarik.png" class="width100" alt="Teh Tarik Session" title="Teh Tarik Session">
    </div>    
</div>
<style>
.footer-div{
	display:none;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br>VOUCHER HAS BEEN SENT TO YOUR EMAIL AS WELL !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>