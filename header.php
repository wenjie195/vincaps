<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype'] == 0)
    //admin
    {
    ?>
        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div admin-header">
<!--                      <div class="dropdown" style="display:inline-block !important;">
                                    <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                Membership <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="adminMembershipPending.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Pending</a></p>
                                        <p class="dropdown-p"><a href="adminMembershipApproved.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Approved</a></p>
                                        <p class="dropdown-p"><a href="adminMembershipRejected.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Rejected</a></p>
										<p class="dropdown-p"><a href="adminMember.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">All Members</a></p>
                                    </div>
                    </div> -->
<!--                      <div class="dropdown" style="display:inline-block !important;">
                                    <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                Blog <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="adminBlogView.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">View</a></p>
                                        <p class="dropdown-p"><a href="adminBlogAdd.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Add</a></p>
                                       
										
                                    </div>
                    </div>   -->
                        <a href="adminContactUsView.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                           Contact Us
                        </a>  
<div class="dropdown" style="display:inline-block !important;">
                                    <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                Blog <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="adminBlogView.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">View Article</a></p>
                                        <p class="dropdown-p"><a href="adminBlogAdd.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Add Article</a></p>
                                       
										
                                    </div>
                    </div>
<div class="dropdown" style="display:inline-block !important;">
                                    <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                              News <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="adminAnnouncementView.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">View Announcement</a></p>
                                        <p class="dropdown-p"><a href="adminAnnouncementAdd.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Add Announcement</a></p>
                                       
										
                                    </div>
                    </div>                    
                                        
                        <!--<a href="adminRegistrationPending.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _MENU_WEBINAR ?>
                        </a>  -->
                             

                         

                                         
                        <a href="logout.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _MENU_LOGOUT ?>
                        </a>  
                            	
                    </div>
                </div>
        </header>
    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    //user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                        <a href="index.php#aboutus" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_ABOUT_US ?>
                        </a>             
                    <div class="dropdown">
                                    <a  class="menu-margin-right menu-item lato">
                                                <?php echo _MENU_PROFILE ?> <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PASSWORD ?></a></p>

                                    </div>
                    </div>                
                        <a href="fundraisingBlog.php" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_BLOG ?>
                        </a>
                        <a href="logout.php" class="menu-margin-right menu-item lato">
                            <?php echo _MENU_LOGOUT ?>
                        </a>  
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger"><?php echo _HEADER_OPEN_MENU ?></button>
                            <ul class="dl-menu">
                                <li><a href="index.php"><?php echo _HEADER_ABOUT_US ?></a></li>
                                <li><a href="profile.php"><?php echo _MENU_PROFILE ?></a></li>    
                                <li><a href="editProfile.php"><?php echo _MENU_EDIT_PROFILE ?></a></li> 
                                <li><a href="editPassword.php"><?php echo _MENU_EDIT_PASSWORD ?></a></li>                             
                                <li><a href="fundraisingBlog.php"><?php echo _HEADER_BLOG ?></a></li>                               
                                <li><a href="logout.php"><?php echo _MENU_LOGOUT ?></a></li>
                            </ul>
                        </div>                	
                    </div>
                </div>
        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 2)
    //premium user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                        <a href="index.php" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_ABOUT_US ?>
                        </a>   
                        
                     
                    <div class="dropdown">
                                    <a  class="menu-margin-right menu-item lato">
                                                <?php echo _MENU_PROFILE ?> <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PASSWORD ?></a></p>

                                    </div>
                    </div>                        
             
                        <a href="fundraisingBlog.php" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_BLOG ?>
                        </a>
                        <a href="logout.php" class="menu-margin-right menu-item lato">
                            <?php echo _MENU_LOGOUT ?>
                        </a>  
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger"><?php echo _HEADER_OPEN_MENU ?></button>
                            <ul class="dl-menu">
                                <li><a href="index.php#aboutus"><?php echo _HEADER_ABOUT_US ?></a></li>
                                <li><a href="profile.php"><?php echo _MENU_PROFILE ?></a></li>    
                                <li><a href="editProfile.php"><?php echo _MENU_EDIT_PROFILE ?></a></li> 
                                <li><a href="editPassword.php"><?php echo _MENU_EDIT_PASSWORD ?></a></li>                             
                                <li><a href="fundraisingBlog.php"><?php echo _HEADER_BLOG ?></a></li>                             
                                <li><a href="logout.php"><?php echo _MENU_LOGOUT ?></a></li>
                            </ul>
                        </div>                	
                    </div>
                </div>
        </header>

    <?php
    }
    ?>

<?php
}
else
{
?>
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                </div>

                <div class="right-menu-div float-right before-header-div">
                    <div class="dropdown menu-item langz" >
                        <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _HEADER_ABOUT_US ?> <img src="img/dropdown.png" class="dropdown-png">
                        </a>
                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="index.php#aboutus"  class="menu-padding dropdown-a menu-item" ><?php echo _HEADER_ABOUT_US ?></a></p>
                            <p class="dropdown-p"><a href="allAnnouncement.php"  class="menu-padding dropdown-a menu-item" ><?php echo _INDEX_IN_THE_NEWS ?></a></p>                            
                            <p class="dropdown-p"><a href="index.php#journey"  class="menu-padding dropdown-a menu-item" ><?php echo _HEADER_OUR_JOURNEY ?></a></p>
                            <p class="dropdown-p"><a href="index.php#performance"  class="menu-padding dropdown-a menu-item" ><?php echo _HEADER_OUR_PERFORMANCE ?></a></p>                                                           
                        </div>
                    </div>   

                    <!-- <a href="allAnnouncement.php" class="menu-margin-right menu-item lato">
                        Announcement
                    </a> -->

                    <div class="dropdown menu-item langz" >
                        <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _HEADER_OUR_SERVICES ?> <img src="img/dropdown.png" class="dropdown-png">
                        </a>
                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="index.php#services"  class="menu-padding dropdown-a menu-item" ><?php echo _HEADER_OUR_SERVICES ?></a></p>
                            <p class="dropdown-p"><a href="index.php#investment"  class="menu-padding dropdown-a menu-item" ><?php echo _HEADER_FOCUS_AREA ?></a></p>
                        </div>
                    </div>     

                    <a href="fundraisingBlog.php" class="menu-margin-right menu-item lato">
                        <?php echo _HEADER_BLOG ?>
                    </a>
                    
 
                    <a href="index.php#contactus" class="menu-margin-right menu-item lato">
                        <?php echo _HEADER_CONTACT_US ?>
                    </a>
                                 

                    <div class="dropdown menu-item langz" >
                                                        <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                                   EN/中文 <img src="img/dropdown.png" class="dropdown-png">
                    
                                                        </a>
                                                        <div class="dropdown-content yellow-dropdown-content">
                                                            <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">English Version</a></p>
                                                            <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">中文版本</a></p>
                                                            
                                                        </div>
                                        </div>        

                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                            <li><a href="index.php#aboutus"><?php echo _HEADER_ABOUT_US ?></a></li>
                            <li><a href="allAnnouncement.php"><?php echo _INDEX_IN_THE_NEWS ?></a></li>
                             <li><a href="index.php#journey"><?php echo _HEADER_OUR_JOURNEY ?></a></li>
                             <li><a href="index.php#performance"><?php echo _HEADER_OUR_PERFORMANCE ?></a></li>
                             <li><a href="index.php#services"><?php echo _HEADER_OUR_SERVICES ?></a></li> 
                             <li><a href="index.php#investment"><?php echo _HEADER_FOCUS_AREA ?></a></li>                             

                            <li><a href="fundraisingBlog.php"><?php echo _HEADER_BLOG ?></a></li>
                            <li><a href="index.php#contactus"><?php echo _HEADER_CONTACT_US ?></a></li>                                        
                            <li class=" langz"><a href="<?php $link ?>?lang=ch">English Version</a></li>
                            <li class=" langz"><a href="<?php $link ?>?lang=en">中文版本</a></li>                            
                            <!--<li><a href="event.php">Event</a></li>-->
<!--                            <li><a class="open-signup">Register</a></li>                                
                            <li><a class="open-login">Login</a></li>-->
                            </ul>
                    </div>
                                            
                </div>
            </div>
    </header>
<?php
}
?>