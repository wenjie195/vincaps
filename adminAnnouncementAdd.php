<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/adminAnnouncementAdd.php" />
<link rel="canonical" href="https://vincaps.com/adminAnnouncementAdd.php" />
<meta property="og:title" content="VinCaps | Admin Add Announcement" />
<title>VinCaps | Admin Add Announcement</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">
<?php include 'css.php'; ?>
</head>

<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding min-height">
	<h1 class="price-h1 dark-blue-text lato">Add New Announcement</h1>

        <!-- <form action="utilities/adminArticlesAddFunction.php" method="POST" enctype="multipart/form-data"> -->
        <form action="utilities/adminAnnouncementAddFunction.php" method="POST" enctype="multipart/form-data">

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Title*</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Title" name="title" id="title" required>      
        </div>   

        <div class="clear"></div>  

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Upload Cover Photo (Less Than 1.8mb)</p>
            <input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
        </div>        

        <div class="clear"></div>

        <div class="width100 overflow ow-margin-top20">
            <p class="input-top-p admin-top-p">Photo Source/Credit (Optional) </p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Photo Source:" name="cover_photo_source" id="cover_photo_source">              	
        </div>        

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Article Summary/Description (Won't Appear inside the Main Content, Avoid "')
                <img src="img/refer.png" class="refer-png pointer opacity-hover open-referde" alt="Article Summary/Description" title="Article Summary/Description">
            </p>
            <textarea class="input-name clean lato blue-text input-textarea admin-input keyword-input desc-textarea" type="text" placeholder="Article Summary" name="descprition" id="descprition" required></textarea>  	
        </div>        

        <div class="clear"></div>      

        <div class="form-group publish-border input-div width100 overflow">
            <p class="input-top-p admin-top-p">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial)
                <img src="img/refer.png" class="refer-png pointer opacity-hover open-refertexteditor" alt="Click Me" title="Click Me">
            </p>
            <textarea name="editor" id="editor" rows="10" cols="80"  class="input-name clean lato blue-text input-textarea admin-input editor-input" ></textarea>
        </div>    

        <div class="clear"></div>    

        <button class="input-submit blue-button white-text clean pointer lato below-forgot margin-bottom30" name="submit">Submit</button>

        </form>

    </div>



<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>
<style>
*{color:#3b6a94;}
</style>
</body>
</html>