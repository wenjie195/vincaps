<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");
$articles = getAnnouncement($conn, " WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/allAnnouncement.php" />
<link rel="canonical" href="https://vincaps.com/allAnnouncement.php" />
<meta property="og:title" content="Vincaps Capital | Announcement" />
<title>Vincaps Capital | Announcement</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">
<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'header.php'; ?>
<div class="background-div">
    <div class="cover-gap content index-content min-height2 overflow">

        <div class="test overflow ">
			<div class="same-padding width100">
        		<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX_IN_THE_NEWS ?></h1>  
			</div>
	<div class="big-white-div padding20 overflow" id="app">
        <?php
        $conn = connDB();
        if($articles)
        {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
            <a href='announcement.php?id=<?php echo $articles[$cnt]->getUid();?>' class="announce-a shadow-hover announcement-card article-card article-card-overwrite  wow fadeIn ow-mh0 " data-wow-delay="0.6s">
                <div class="">
                    <div class="square progressive square-news">
                    <div class="content2">
               
                
                <img data-src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" src="img/preload.jpg" class="width100 preview lazy" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>"/>

            </div>  </div>  
					 
           <div class="box-caption box2 box2-right">
            <div class="wrap-a wrap100">
            <span class="light-blue-text small-date"><?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?></span>
            </div>
                            
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a dark-blue-text text-overflow">
                                    <?php echo $articles[$cnt]->getTitle();?>
                                </div>

                                <div class="text-content-div dark-blue-text text-content-div-mh">
                                    <?php echo $description = $articles[$cnt]->getKeywordOne();?>
                                </div>
								
                            </div>
                            
                        </div>
                    </a>            
            
            
            

        <?php
        }
        ?>
        <?php
        }
        $conn->close();
        ?>
	</div>

</div>
</div>
<?php include 'js.php'; ?>

</body>
</html>