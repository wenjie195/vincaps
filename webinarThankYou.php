<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/webinarThankYou.php" />
<link rel="canonical" href="https://vincaps.com/webinarThankYou.php" />
<meta property="og:title" content="VinCaps | Thank You" />
<title>VinCaps | Thank You</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="two-bg-container overflow">
        <div class="top-building-div width100"></div>
        <div class="bottom-water-div width100"></div>
	</div>
    <div class="big-container-css width100 ow-big-container-css">
      <div class="blue-div-opa width100 small-padding">
      	<!--<span class="close-css inner-close  upper-close">&times;</span>-->
      	<img src="img/thank-you.png" class="hello-icon blue-icon2" alt="Thank You" title="Thank You">
      	<h1 class="white-text welcome lato welcome2">Thank You</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     
    
            <p class="lato blue-text explanation-p text-center thankyou-p">Thanks for register the webinar.</p>
        	<a href="index.php"><div class="input-submit blue-button white-text clean pointer lato below-forgot text-center" >Explore More</div></a>

    </div>
  </div>

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}

</style>
<?php include 'js.php'; ?>


</body>
</html>