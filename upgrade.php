<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/upgrade.php" />
<link rel="canonical" href="https://vincaps.com/upgrade.php" />
<meta property="og:title" content="VinCaps | Upgrade" />
<title>VinCaps | Upgrade</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="two-bg-container overflow">
        <div class="top-building-div width100"></div>
        <div class="bottom-water-div width100"></div>
	</div>
    <div class="big-container-css width100">
      <div class="blue-div-opa width100 small-padding">
      	<!--<span class="close-css inner-close  upper-close">&times;</span>-->
      	<img src="img/premium-member.png" class="hello-icon blue-icon2" alt="Premium Member" title="Premium Member">
      	<h1 class="white-text welcome lato welcome2">Premium Member</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     
          <!-- <form method="POST"  action=""> -->
          <form action="utilities/subscribeFunction.php" method="POST" enctype="multipart/form-data">
    
            <p class="lato blue-text explanation-p">Upgrade to be our <b class="blue-text lato">Premium Member</b> now with only RM500 (for 1 year subscription) and you can access to our:</p>
        	<table class="tick-table lato">
            	<tbody>
                	<tr >
                    	<td class="tick-td"><img src="img/tick.png" class="tick-png"></td>
                        <td class="light-blue-text">VinCaps Assessment</td>
                    </tr>
                	<tr>
                    	<td class="tick-td"><img src="img/tick.png" class="tick-png"></td>
                        <td class="light-blue-text">Exclusive Insight</td>
                    </tr>                    
                	<tr>
                    	<td class="tick-td"><img src="img/tick.png" class="tick-png"></td>
                        <td class="light-blue-text">Video</td>
                    </tr> 
                	<tr>
                    	<td class="tick-td"><img src="img/tick.png" class="tick-png"></td>
                        <td class="light-blue-text">eBook</td>
                    </tr>                    
                	<tr>
                    	<td class="tick-td"><img src="img/tick.png" class="tick-png"></td>
                        <td class="light-blue-text">Podcast</td>
                    </tr>                    
                	<tr>
                    	<td class="tick-td"><img src="img/tick.png" class="tick-png"></td>
                        <td class="light-blue-text">Resources Library</td>
                    </tr>                     
                    
                                        
                </tbody>
            </table>
           <div class="clear"></div> 
           <div class="light-blue-line width100"></div> 
            <p class="lato blue-text explanation-p">Kindly bank in RM500.00 to <b class="blue-text">one</b> of the bank accounts below:</p>
            <p class="input-top-p input-top-p2">Bank Account 1</p>
            <p class="lato blue-text explanation-p explanation-p2">RHB Bank</p>
            <p class="input-top-p input-top-p2">Bank Account No.</p>
            <p class="lato blue-text explanation-p explanation-p2"><input type="text" value="20210800032819" class="lato blue-text explanation-p explanation-p2 clean no-input" id="rhb"><button onclick="copy()" class="blue-button clean copy-button">Copy</button></p>
            <p class="input-top-p input-top-p2">Account Holder</p>
            <p class="lato blue-text explanation-p explanation-p2">Vincaps Sdn Bhd</p>
            <div class="light-blue-line width50"></div> 
            <p class="input-top-p input-top-p2">Bank Account 2</p>
            <p class="lato blue-text explanation-p explanation-p2">UOB Bank</p>
            <p class="input-top-p input-top-p2">Bank Account No.</p>
            <p class="lato blue-text explanation-p explanation-p2"><input type="text" value="2263033525" class="lato blue-text explanation-p explanation-p2 clean no-input" id="uob"> <button onclick="copyUOB()" class="blue-button clean copy-button">Copy</button></p>
            <p class="input-top-p input-top-p2">Account Holder</p>
            <p class="lato blue-text explanation-p explanation-p2">VinCaps Sdn Bhd</p>
            <div class="light-blue-line width100"></div>    
            <p class="lato blue-text explanation-p">Fill in the details below if you completed bank in the RM500. We will take 1 - 2 working days to go through and activate your premium membership.</p>
			<p class="input-top-p">Which Bank You Banked In To?</p>
            <select class="input-name clean lato blue-text" id="bank_selection" name="bank_selection" required>
            	<option></option>
            	<option value="RHB Bank - 20210800032819" name="RHB Bank - 20210800032819" class="lato blue-text">RHB Bank - 20210800032819</option>
                <option value="UOB Bank - 2263033525" name="UOB Bank - 2263033525" class="lato blue-text">UOB Bank - 2263033525</option>
            </select>            
            <p class="input-top-p">Payment Method</p>
            <select class="input-name clean lato blue-text" id="package_selection" name="package_selection" required>
            	<option></option>
            	<option value="Online Banking/Bank Transfer" name="Online Banking/Bank Transfer" class="lato blue-text">Online Banking/Bank Transfer</option>
                <option value="Credit Card" name="Credit Card" class="lato blue-text">Credit Card</option>
                <option value="ATM/Cash Deposit" name="ATM/Cash Deposit" class="lato blue-text">ATM/Cash Deposit</option>
            </select>  
            <p class="input-top-p">Your Bank Account Holder Name</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Your Bank Account Holder Name" id="bank_holder" name="bank_holder" required>            
            <p class="input-top-p">Recipient Reference</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Subscribing Premium Membership" id="reference" name="reference" required>            
            <p class="input-top-p">Upload Receipt</p>  
            <input type="file" id="file_one" name="file_one" required>   
            <!--

                
            <p class="input-top-p">Email</p>
            <input class="input-name clean" type="email" placeholder="Email" id="email" name="email" required>

			<p class="input-top-p">Contact Number</p>
            <input class="input-name clean" type="text" placeholder="Contact Number" id="register_contact" name="register_contact" required>

			<p class="input-top-p">Company Name</p>
            <input class="input-name clean" type="text" placeholder="Company Name" id="" name="" required>
    
            <p class="input-top-p">Password</p>
            <div class="fake-input-div overflow">
            	<input class="input-name clean password-input" type="password" placeholder="Password" id="" name="" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
    		</div>
 
            <p class="input-top-p">Retype Password</p>
            <div class="fake-input-div overflow">
            	<input class="input-name clean password-input" type="password" placeholder="Retype Password" id="" name="" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
    		</div> -->
    		
            <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="register">Confirm Payment</button>
    		<p class="signup-p text-center"><a onclick="goBack()" class="blue-link lato text-center signup-a">Not now</a></p>
            <div class="light-blue-line width100"></div> 
            <p class="lato blue-text explanation-p text-center">If you have any queries, kindly <a href="index.php#contactus" target="_blank" class="light-blue-link">contact us.</a></p>
    
          </form>
    </div>
  </div>
<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}

</style>
<?php include 'js.php'; ?>


</body>
</html>