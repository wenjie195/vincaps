<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

// $pendingMember = getPayment($conn, "WHERE status = 'Approved' ");
$allUser = getUser($conn, "WHERE user_type = 1 ");
$allPremiumUser = getUser($conn, "WHERE user_type = 2");
// $allUser = getUser($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/adminMember.php" />
<link rel="canonical" href="https://vincaps.com/adminMember.php" />
<meta property="og:title" content="VinCaps | Member" />
<title>VinCaps | Member</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

    <h1 class="price-h1 dark-blue-text lato">Normal Members</h1>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">
        <table class="approve-table lato">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Company Name</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allUser)
                    {
                        for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allUser[$cnt]->getFullname();?></td>
                                <td><?php echo $allUser[$cnt]->getEmail();?></td>
                                <td><?php echo $allUser[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $allUser[$cnt]->getCompanyName();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>
    </div>  

    <div class="clear"></div>

    <h1 class="price-h1 dark-blue-text lato">Premium Members</h1>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">
        <table class="approve-table lato">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Company Name</th>
                        <th>Expiry Date</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allPremiumUser)
                    {
                        for($cnt = 0;$cnt < count($allPremiumUser) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allPremiumUser[$cnt]->getFullname();?></td>
                                <td><?php echo $allPremiumUser[$cnt]->getEmail();?></td>
                                <td><?php echo $allPremiumUser[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $allPremiumUser[$cnt]->getCompanyName();?></td>
                                <td><?php echo $allPremiumUser[$cnt]->getExpired();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>
    </div>  

</div>

<?php include 'js.php'; ?>

</body>
</html>