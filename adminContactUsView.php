<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/Package.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $pendingMember = getPayment($conn, "WHERE status = 'Pending' ");
$allPackage = getPackage($conn, " WHERE status = 'Display' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/adminBlogView.php" />
<link rel="canonical" href="https://vincaps.com/adminBlogView.php" />
<meta property="og:title" content="VinCaps | Admin Contact Us" />
<title>VinCaps | Admin Contact Us</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

    <h1 class="price-h1 dark-blue-text lato">All Data</h1>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30" style="margin-bottom:30px;">
  
        <table class="approve-table lato">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>

                        <th>Status</th>
                        <th>Newsletter</th>
                        <th>Message</th>
                        <th>Date</th>

                        <th>Delete</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allPackage)
                    {
                        for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allPackage[$cnt]->getUsername();?></td>
                                <td><?php echo $allPackage[$cnt]->getEmail();?></td>

                                <td><?php echo $allPackage[$cnt]->getPhone();?></td>
                                <td><?php echo $allPackage[$cnt]->getPackage();?></td>
                                <td><?php echo $allPackage[$cnt]->getNewsletter();?></td>

                                <td>
                                    <!-- <form action="#" method="POST"> -->
                                    <form action="adminContactUsViewMessage.php" method="POST">
                                        <button class="clean blue-link2 transparent-button pointer" type="submit" name="item_uid" value="<?php echo $allPackage[$cnt]->getUid();?>">View</button>
                                    </form>
                                </td>

                                <td>
                                    <?php echo $date = date("d/m/Y",strtotime($allPackage[$cnt]->getDateCreated()));?>
                                </td>

                                <td>
                                    <form method="POST" action="utilities/adminContactUsDeleteFunction.php" class="hover1">
                                        <button class="clean transparent-button" type="submit" name="item_uid" value="<?php echo $allPackage[$cnt]->getUid();?>">
                                            <img src="img/reject.png" class="approval-icon opacity-hover pointer" alt="Delete" title="Delete">
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Data Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>