<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $transfer_uid = rewrite($_POST["transfer_uid"]);
    // $type = "2";
    $status = "Rejected";

    // // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $transfer_uid."<br>";

    $upgradeMembership = getPayment($conn," WHERE uid = ? ",array("uid"),array($transfer_uid),"s");   

    if($upgradeMembership)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$transfer_uid);
        $stringType .=  "s";
        $statusUpdated = updateDynamicData($conn,"payment"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($statusUpdated)
        {
            echo "<script>alert('membership rejected successfully !!');window.location='../adminMembershipPending.php'</script>";  
        }
        else
        {
            echo "<script>alert('fail to reject membership !!');window.location='../adminMembershipPending.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminMembershipPending.php'</script>";  
    }
}
else 
{
    header('Location: ../index.php');
}
?>