-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2021 at 07:30 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_vincaps`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '65e30045c6da4c6cf57e0c0a1423676b', '528d6cb31e16abb36c1365094e5c9183', 'admin', 'VinCaps Article 1', 'VinCapsArticle1-vincaps-article-one', 'vincaps-article-one', '传陈奕迅双11晚会　唱3首歌收百万', 'vincaps, article, one, keyword', '65e30045c6da4c6cf57e0c0a1423676bmmexport1573396227303.jpg', '<h1 style=\"text-align:center\">（浙江11日讯）双11是全球网民准备剁手的大日子，而这个大日子也请了不少大咖艺人前来助阵，周二晚上举办的《天猫双11狂欢夜》，邀请了陈奕迅等人演唱，他一连为大家带来了3首歌表演，包括《陪你度过漫长的岁月》﹑《谢谢侬》，以及全新派台歌《致明日的舞》，在场观众都大呼耳朵怀孕。</h1>\r\n\r\n<p><img alt=\"陈奕迅用美声征服全场，最重要的是终于有现场演出，不再钱包干硬化。\" src=\"https://www.orientaldaily.com.my/images/uploads/news/2020/NOV_2020/20201111/eleven07.jpg\" /></p>\r\n\r\n<p><span style=\"font-size:18px\">身穿全白色上衣配西裤打扮的陈奕迅一出场即获热烈欢呼声，之后立即用美声政府全场，当唱跳完之后就唱广东歌《致明日的舞》，公开向大家传递正能量，表明每个人都有遇到不开心的时候，最重要的是在不开心中可以学习到甚么。</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:18px\">早前陈奕迅为新歌宣传时，就透露今年因为受疫情的影响，导致许多工作计划受到影响，更笑言今年目前为止只拍了一个快餐店广告和新歌录音工作，因此收入大减，更搞笑说自己患上了&ldquo;钱包干硬化&rdquo;同&ldquo;急性发钱寒&rdquo;的症状，之后一度成为年度金句。</span></p>\r\n\r\n<p><span style=\"font-size:18px\">据闻，这次他为《天猫双11狂欢夜》担任表演嘉宾，有传他的商演价目是180万人民币（约112万令吉），也可以拯救他干硬化的钱包。</span></p>\r\n\r\n<p><img alt=\"陈奕迅载歌载舞，掀起全场高潮。\" src=\"https://www.orientaldaily.com.my/images/uploads/news/2020/NOV_2020/20201111/eleven06.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, NULL, NULL, NULL, NULL, 'vincaps photo source', NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-11-11 03:44:52', '2020-11-11 03:44:52'),
(2, '0746372be5c926dd8665aa4e833ebcc1', '528d6cb31e16abb36c1365094e5c9183', 'admin', 'VinCaps Article 2', 'VinCapsArticle2-vincaps-article-two', 'vincaps-article-two', '如何看待媒体收错料', 'vincaps, article, two, keyword', '0746372be5c926dd8665aa4e833ebcc192495735_1137381663267797_575326391311007744_n.png', '<p style=\"text-align:center\">网络媒体《透视大马》本周有一篇新闻，引述&ldquo;消息人士&rdquo;指慕尤丁要闪电大选，而国安会有异议。其实早在三个月前，疫情整体上已经受到控制，加上政府在行管期发放的援助金确实让不少人&ldquo;感觉良好&rdquo;，民调显示他人气直升，坊间就有传言慕尤丁想举行大选。当时《星洲日报》也以独家的形式报道。这些报道是真是假，确实引起一些人讨论甚至嘲笑。</p>\r\n\r\n<p style=\"text-align:justify\">在没有固定大选日期的国家，任何执政者都会选择对自己有利的时机解散国会，重新寻求委托。因此，看回几个月前利好的情况，慕尤丁有大选的念头，并不让人意外。英国在2011年通过固定任期国会法案以前，首相也都会尽量挑选有利于己的日期大选，通常是经济一片大好，又或是民调显示执政党遥遥领先在野党之时，图的就是所谓的利好因素能够为自己加分。除非逼不得已，没有首相会选择不利于己的时刻大选。</p>\r\n\r\n<p>1997年，党内外交困的梅杰就是因为民望低落，拖到最后一刻才解散国会，在当时是将近100年来少有做满五年任期才大选的首相（二战期间冻结大选不算），结果当然是一败涂地。在那个时候，每当首相声望如日中天，或执政党焦头烂额的时候，各家报章媒体都会争先恐后预测大选日期或首相会否半路下车，而且这不只是只有唯恐天下不乱的小报才会做的事，大报也是如此，引述的当然是所谓的可靠消息；有的甚至很笃定地写&ldquo;XXX（报章）可以披露&rdquo;（canreveal）。</p>\r\n\r\n<p>媒体喜欢这么做，原因很多。一来民众热衷猜测何时大选，二来可以测试政府的反应，甚至企图动摇首相在党内的地位，例如梅杰就被媒体玩得很惨；工党籍的首相布莱尔和他的财长布朗不咬弦，也是当时媒体借用大选或交棒日期，极尽挑拨离间之能事的材料。当年类似这样的新闻很多。严肃的，我多看几眼；耸动的，我淡然处之。</p>\r\n\r\n<p style=\"text-align:right\">因此，英国首相向君主建议解散国会的传统，长期被视为一种不民主的优势，也不符合三权分立的精神，毕竟行政长官不应该掌握&ldquo;炒立法机关鱿鱼&rdquo;的权力，因此才会通过固定任期国会法案，确立了国会的五年任期和选举日期。首相失去了随意解散国会的绝对权力，若想提早解散国会，必须得到下议院2/3议员同意，或另立一次性新法，不然就是因为不信任动议通过而被迫大选。</p>\r\n\r\n<p style=\"text-align:right\">2016年6月，卡梅伦输掉脱欧公投下台，特丽莎梅接任，民调显示她人气急升，那时候媒体就猜测会不会闪电大选。特丽莎梅先是否认，过后突然到下议院寻求表决解散国会，结果烧掉了卡梅伦留给她的国会多数。但我们总不能说她在否认的那一刻是在散播假消息吧？</p>\r\n\r\n<p style=\"text-align:right\">&nbsp;</p>\r\n\r\n<p style=\"text-align:right\"><img src=\"https://www.orientaldaily.com.my/storage/resize_cache/images/uploads/news-cover/2020/NOV_2020/20201110/getty_954321588_406856.jpg/49cca8e6255e3915861ccfc222bc67aa.jpg\" style=\"float:left\" /></p>\r\n', NULL, NULL, NULL, NULL, NULL, 'vincaps photo source two', NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-11-11 03:48:37', '2020-11-11 03:48:37');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_holder` varchar(255) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uid`, `user_uid`, `username`, `email`, `phone_no`, `company_name`, `amount`, `package`, `bank`, `bank_holder`, `bank_reference`, `receipt`, `duration`, `manufactured`, `expired`, `status`, `date_created`, `date_updated`) VALUES
(1, '2657df5ae8d741c1c71e3fea1c395358', 'ea6182d593b150753933c58b01040bd6', 'user', 'user@gmail.com', '011445566', 'Test Company', NULL, 'Online Banking/Bank Transfer', 'RHB Bank - 20210800032819', 'user', '7744110', '1605079263testreceipt.jpg', NULL, NULL, NULL, 'Rejected', '2020-11-11 07:21:03.6', '2020-11-11 07:27:54.7'),
(2, 'af174ea99ad0e16ae2e0497d68e6d41e', '9654080b675074125dc45a6699a7d3de', 'oliver', 'oliver@gmail.com', '011123123', 'Queen Consolidated', NULL, 'Credit Card', 'UOB Bank - 2263033525', 'oliver queen', '8855220', '1605079326mmexport1573396227303.jpg', '365 days', '2020-11-11', '2021-11-11', 'Approved', '2020-11-11 07:22:06.5', '2020-11-11 07:29:52.3'),
(3, '8b05b00958f71de69c55c63da6d20cd4', '476bc8b154abbb53dec8d8bc68ac98a7', 'barry', 'barry@gmail.com', '0127744520', 'Star Labs', NULL, 'ATM/Cash Deposit', 'RHB Bank - 20210800032819', 'allen', '4455663', '1605079396dc motor.png', NULL, NULL, NULL, 'Pending', '2020-11-11 07:23:16.3', '2020-11-11 07:23:16.3');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(255) DEFAULT NULL COMMENT 'For login probably if needed',
  `email` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `user_type` int(2) DEFAULT NULL COMMENT '0 = admin, 1 = normal user',
  `remark_one` varchar(255) DEFAULT NULL,
  `remark_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `fullname`, `company_name`, `duration`, `manufactured`, `expired`, `status`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '528d6cb31e16abb36c1365094e5c9183', 'admin', 'admin@gmail.com', '074bb3bdb8b3791688fb4af87b339aeea0c2867b7f12df8b47f35f7384396624', '9de0f39a26c88b978964c426c9fc89db0f66028a', '012123123', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-06 03:55:51', '2020-11-09 09:26:04'),
(2, 'ea6182d593b150753933c58b01040bd6', 'user', 'user@gmail.com', '074bb3bdb8b3791688fb4af87b339aeea0c2867b7f12df8b47f35f7384396624', '9de0f39a26c88b978964c426c9fc89db0f66028a', '011445566', 'user', 'Test Company', NULL, NULL, NULL, NULL, 1, '2020-11-06 04:00:21', '2020-11-11 07:27:10'),
(3, '9654080b675074125dc45a6699a7d3de', 'oliver', 'oliver@gmail.com', '802deb1dcaa4e04933e45e217df0daa8bf2b379a666d29d5761975a02d554bb4', 'b0144665481863b8a681f389bf38c1f55fe5eafc', '011123123', 'oliver', 'Queen Consolidated', '365 days', '2020-11-11', '2021-11-11', 'Approved', 2, '2020-11-09 09:25:46', '2020-11-11 08:11:46'),
(5, '476bc8b154abbb53dec8d8bc68ac98a7', 'barry', 'barry@gmail.com', '7694adb6fc142eb623249d2935873559affc3f87f71a42270c4890925ec57b48', '1cfdcdbfd2b94fb0eabac56ffa81ec0d93c83430', '0127744520', 'barry', 'Star Labs', NULL, NULL, NULL, NULL, 1, '2020-11-11 07:19:34', '2020-11-11 07:19:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
