<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<meta property="og:image" content="https://vincaps.com/img/pydc-meta.jpg" />
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/event.php" />
<link rel="canonical" href="https://vincaps.com/event.php" />
<meta property="og:title" content="Penang Start-Up Accelerator Program" />
<title>Penang Start-Up Accelerator Program</title>
<meta property="og:description" content="When looking to grow your business, you need to look for the right people for network and funding." />
<meta name="description" content="When looking to grow your business, you need to look for the right people for network and funding." />
<meta name="keywords" content="teh tarik, vincaps, kevin, Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
<meta property="og:image" content="https://vincaps.com/img/pydc-meta.jpg" />
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow top-blue-div same-padding">
	
		<h1 class="blue-benner-h1 white-text text-center wow fadeIn" data-wow-delay="0.2s"><?php echo _INDEX2_BLUE_QUOTE ?></h1>
        <p class="people-name white-text text-center wow fadeIn" data-wow-delay="0.4s">1. Y.B. Soon Lip Chee, Penang State Executive Council for Youth and Sports<br>
2. Kevin Yam, Managing Partner of Vincaps Capital <br>
3. Henry Tye , Founder of Big Domain <br>
4. Dr Gwee Sai Ling, General Manager of The
Penang Youth Development Corporation (PYDC)</p>
    

</div>
<div class="clear"></div>
<div class="width100 overflow second-div1 same-padding">
    		
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s"><?php echo _INDEX2_WHO_ARE ?></h1>
            <p class="about-p2 p-size black-text wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX2_WHO_ARE2 ?></p>
            <div class="clear"></div>
            <div class="text-center overflow logo-div1">
           		 <img src="img/bd.png" class="logo-div-png wow fadeIn" data-wow-delay="0.6s">
            	<img src="img/p1.png" class="logo-div-png wow fadeIn" data-wow-delay="0.6s">
                <img src="img/p2.png" class="logo-div-png wow fadeIn" data-wow-delay="0.8s">
                <img src="img/p3.png" class="logo-div-png logo-div-png-third wow fadeIn" data-wow-delay="1s">
                <img src="img/vincaps-logo.png" class="logo-div-png wow fadeIn" data-wow-delay="1.2s">
                <img src="img/p5.png" class="logo-div-png logo-div-png-last wow fadeIn" data-wow-delay="1.4s">
            </div>
</div>
<div class="clear"></div>
<div class="width100 overflow second-div1 same-padding">
    		
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s"><?php echo _INDEX2_WHAT_WE_DO ?></h1>
            <p class="about-p2 p-size black-text wow fadeIn" data-wow-delay="0.4s"><?php echo _INDEX2_WHAT_WE_DO2 ?></p>
                    <div class="clear"></div>    
     		<div class="text-center four-div-css">
                <img src="img/prof.png" class="about-png wow fadeIn" data-wow-delay="0.6s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX2_DO1 ?></p>
        	</div>
     		<div class="text-center four-div-css">
                <img src="img/presentation.png" class="about-png wow fadeIn" data-wow-delay="1s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.2s"><?php echo _INDEX2_DO2 ?></p>
        	</div>            
     		<div class="text-center four-div-css">
                <img src="img/insight.png" class="about-png wow fadeIn" data-wow-delay="1.4s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.6s"><?php echo _INDEX2_DO3 ?></p>
        	</div>                
     		<div class="text-center four-div-css">
                <img src="img/slogan.png" class="about-png wow fadeIn" data-wow-delay="1.8s">
                <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2s"><?php echo _INDEX2_DO4 ?></p>
        	</div>  
            <div class="clear"></div>
           
</div>
    <div class="clear"></div> 

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
       
    	<p class="icon-p1 text-center"><img src="img/call.png" class="header-icon wow fadeIn" data-wow-delay="0.2s"></p>
        	<h1 class="line-header wow fadeIn" data-wow-delay="0.4s"><?php echo _HEADER_CONTACT_US ?></h1>    
    	<div class="clear"></div>      
        
<div class="five-col float-left two-column-css">
  <form class="form-class extra-margin" action="utilities/submitContactUsFunction2.php" method="POST">
    <input type="text" name="name" placeholder="<?php echo _INDEX_NAME ?>" class="input-name clean dark-blue-text wow fadeIn" data-wow-delay="0.6s" required >
    <input type="email" name="email" placeholder="<?php echo _INDEX_EMAIL ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="0.8s" required >
    <input type="text" name="telephone" placeholder="<?php echo _INDEX_CONTACT_NUMBER ?>" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.0s" required >
    
    <select  class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.2s" id="selection" name="selection" required>
      <option class=" clean lato dark-blue-text" value='Investor'><?php echo _INDEX2_INVESTOR ?></option>
      <option class=" clean lato dark-blue-text" value='Business Owner'><?php echo _INDEX2_B_OWNER ?></option>
    </select> 

    <textarea name="comments" placeholder="<?php echo _INDEX_MESSAGE ?>" class="input-name input-message clean lato dark-blue-text wow fadeIn" data-wow-delay="1.4s" ></textarea>
    <div class="clear"></div>
    <table class="form-table">
      <tbody>
        <tr class="wow fadeIn" data-wow-delay="1.6s">
          <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_UPDATED_WITH ?></p></td>
        </tr>
        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
          <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_I_JUST_WANT ?></p></td>
        </tr>
      </tbody>
    </table>
    <div class="res-div">
      <input type="submit" name="send_email_button" value="<?php echo _INDEX_SEND ?>" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="1.8s">
    </div>
  </form>
</div>	

        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2s">
                Contact Point<br>
                <b class="dark-blue-text weight900">samantha@pydc.com.my</b>
        	</p>         
			<p class="contact-p light-blue-text wow fadeIn" data-wow-delay="2.2s">
                <?php echo _INDEX_CONTACT_NUMBER ?><br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60149016968">0149016968</a>/<a class="dark-blue-text weight900" href="tel:+60124010182">0124010182</a></b>
        	</p> 

                     
        </div> 
</div>
<div class="clear"></div>  



<div class="clear"></div>   

    
<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>


</body>
</html>