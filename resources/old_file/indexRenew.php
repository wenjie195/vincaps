<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="VinCaps | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?>" />
<title>VinCaps | <?php echo _INDEX_YOUR_STARTUP_FUNDING ?></title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<!--    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssorb052 .i {position:absolute;cursor:pointer;}
        .jssorb052 .i .b {fill:#245477;fill-opacity:0.3;}
        .jssorb052 .i:hover .b {fill-opacity:.7;}
        .jssorb052 .iav .b {fill-opacity: 1;}
        .jssorb052 .i.idn {opacity:.3;}

        .jssora053 {display:block;position:absolute;cursor:pointer; width:25px !important; height:25px !important; opacity:0.8;}
        .jssora053 .a {fill:none;stroke:#fff;stroke-width:640;stroke-miterlimit:10;}
        .jssora053:hover {opacity:.6;}
        .jssora053.jssora053dn {opacity:.5;}
        .jssora053.jssora053ds {opacity:.3;pointer-events:none;}
    </style>-->
<?php include 'header.php'; ?>
<div class="width100 overflow">
<div id="fly-in">  
<div><span class="span1"><?php echo _INDEX_SENTENCE1 ?></span></div>
<div><span class="span1"><?php echo _INDEX_SENTENCE2 ?></span></div>
<div><span class="span1"><?php echo _INDEX_SENTENCE3 ?></span></div>
<div><span class="span1"><?php echo _INDEX_SENTENCE4 ?></span></div>
<div><span class="span1"><?php echo _INDEX_SENTENCE5 ?></span></div>
<div><span class="span1"><?php echo _INDEX_SENTENCE6 ?></span></div>
</div>
<div  class="container light-particle-div">
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
  <div class="circle-container">
    <div class="circle"></div>
  </div>
</div>
</div>
<!--   <div id="jssor_1" class="slider-css" >

        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
            <div>
                <img data-u="image" src="img/banner01.jpg" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="img/banner02.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/banner03.jpg" />
            </div>

        </div>
        <!-- Bullet Navigator -->
        <!--<div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        
        <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>-->
  <div class="clear"></div>  
 <div class="wrapper width100 overflow same-padding">
         
        <p class="blue-text lato top-des float-left ow-section-des" ><?php echo _INDEX_GLOBAL_REACH ?></p>
        <div class="line-startup ow-section-line float-right"></div>
        <div class="clear"></div>
        <div class="text-center">
        	<img src="img/raise-fund.png" class="title-icon">
        </div>
 			<h1 class="title-ch1 text-center dark-blue-text">RM<span class="timer count-title dark-blue-text" id="count-number" data-to="5.94" data-speed="2500"></span><?php echo _INDEX_MILLION_RAISED ?></h1>
    <div class="counter text-center three-counter">
     <img src="img/government-grants.png" class="counter-icon">
      <h2 class="dark-blue-text counter-h2">RM<span class="timer count-title dark-blue-text" id="count-number" data-to="8.94" data-speed="2500"></span><?php echo _INDEX_MILLION ?></h2>
       <p class="count-text light-blue-text"><?php echo _INDEX_TOTAL_CAPITAL_PASSED ?></p>
    </div>

    <div class="counter text-center three-counter middle-three-counter">
      <img src="img/equity-crowdfunding.png" class="counter-icon">
      <h2 class="dark-blue-text counter-h2">RM<span class="timer count-title dark-blue-text" id="count-number" data-to="287" data-speed="2500"></span><?php echo _INDEX_MILLION ?></h2>
      <p class="count-text light-blue-text"><?php echo _INDEX_TOTAL_OF_DEALS ?></p>
    </div>

    <div class="counter text-center three-counter">
      <img src="img/job.png" class="counter-icon">
      <h2 class="dark-blue-text counter-h2">RM<span class="timer count-title dark-blue-text" id="count-number" data-to="500" data-speed="2500"></span><?php echo _INDEX_MILLION ?></h2>
      <p class="count-text light-blue-text"><?php echo _INDEX_A_RM500 ?></p>
    </div>

    <div class="counter text-center three-counter">
      <img src="img/shake-hands.png" class="counter-icon">
      <h2 class="dark-blue-text counter-h2">RM<span class="timer count-title dark-blue-text" id="count-number" data-to="20" data-speed="2500"></span><?php echo _INDEX_MILLION ?></h2>
      <p class="count-text light-blue-text"><?php echo _INDEX_MOST_RECENT ?></p>
    </div>
    <div class="counter text-center three-counter">
      <img src="img/family-office.png" class="counter-icon">
      <h2 class="dark-blue-text counter-h2"><span class="timer count-title dark-blue-text" id="count-number" data-to="7.5" data-speed="2500"></span>%</h2>
      <p class="count-text light-blue-text"><?php echo _INDEX_COMMISSIONS_PERCEN ?></p>
    </div>

    <div class="counter text-center three-counter">
      <img src="img/global.png" class="counter-icon">
      <h2 class="dark-blue-text counter-h2"><span class="timer count-title dark-blue-text" id="count-number" data-to="3" data-speed="2500"></span>%</h2>
      <p class="count-text light-blue-text"><?php echo _INDEX_COMMISSIONS_PERCEN_FROM ?></p>
    </div>    
</div>     
 <div class="clear"></div>
 
<div class="same-padding width100 white-spacing-div overflow" id="app">

        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn" data-wow-delay="0.2s" >VinCaps <?php echo _INDEX_BLOG ?></p>
        <div class="line-startup ow-section-line float-right wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>

	<div class="clear"></div>
        <?php
        $conn = connDB();
        if($articles)
        {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
        <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
            <div class="article-card article-card-overwrite opacity-hover wow fadeIn" data-wow-delay="0.6s">
           
            <div class="article-bg-img-box progressive">
            	
  					<img data-src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" src="img/preload.jpg" class="preview width100 lazy" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>"/>
				</a>
           </div>           
           <div class="box-caption box2">
            <div class="wrap-a wrap100">
            <span class="light-blue-text small-date"><a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="light-blue-text small-date"><?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?></span>
            </div>
 <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a dark-blue-text">
                                    <?php echo $articles[$cnt]->getTitle();?>
                                </div>

                                <div class="text-content-div dark-blue-text">
                                    <?php echo $description = $articles[$cnt]->getKeywordOne();?>

                                </div>
								</a>
                            </div>
                            
                        </div>
                    </a>    
        <?php
        }
        ?>
        <?php
        }
        $conn->close();
        ?>


</div>  
<div class="clear"></div>
 <div class="width100 overflow same-padding light-blue-bg relative">
         
        <p class="blue-text lato top-des float-left ow-section-des timeline-ow wow fadeIn" data-wow-delay="0.3s" ><?php echo _INDEX_PROGRESSIVE_TIMELINE ?></p>
        <div class="line-startup ow-section-line float-right timeline-line wow fadeIn" data-wow-delay="0.6s"></div>
        <div class="clear"></div> 
        <div class=" follow-harm-width margin-top50">
            <div class="grey-text timeline-item wow fadeIn" data-wow-delay="0.9s" date-is='<?php echo _INDEX_EST_DEC_2023 ?>'>
                
                <p class="grey-text"><?php echo _INDEX_REACH ?> <b>RM10<?php echo _INDEX_BIL ?></b> <?php echo _INDEX_DEAL ?></p>
            </div>
            
            <div class="grey-text timeline-item wow fadeIn" data-wow-delay="1.2s" date-is='<?php echo _INDEX_EST_DEC_2021 ?>'>
                
                <p class="grey-text"><?php echo _INDEX_REACH ?> <b>RM1<?php echo _INDEX_BIL ?></b> <?php echo _INDEX_DEAL ?></p>
            </div>
            <div class="timeline-item wow fadeIn" data-wow-delay="1.5s" date-is='<?php echo _INDEX_TWENTY_MARCH ?>'>
                
                <p><?php echo _INDEX_REACH ?> <b>RM100<?php echo _INDEX_MIL ?></b> <?php echo _INDEX_DEAL ?> <img src="img/double-tick.png" class="double" alt="Archived" title="Archived"></p>
            </div>	
            <div class="timeline-item wow fadeIn" data-wow-delay="1.8s" date-is='<?php echo _INDEX_SECOND_FEB ?>'>
                
                <p><?php echo _INDEX_REACH ?> <b>RM10<?php echo _INDEX_MIL ?></b> <?php echo _INDEX_DEAL ?> <img src="img/double-tick.png" class="double" alt="Archived" title="Archived"></p>
            </div>     
            <div class="timeline-item wow fadeIn" data-wow-delay="2.1s" date-is='<?php echo _INDEX_THIRTY_SEP ?>'>
                
                <p><?php echo _INDEX_REACH ?> <b>RM1<?php echo _INDEX_MIL ?></b> <?php echo _INDEX_DEAL ?> <img src="img/double-tick.png" class="double" alt="Archived" title="Archived"></p>
            </div>            
            
        </div>        
         <div class="ripple-background wow fadeIn" data-wow-delay="2.4s" >
          <div class="circle-ri xxlarge shade1"></div>
          <div class="circle-ri xlarge shade2"></div>
          <div class="circle-ri large shade3"></div>
          <div class="circle-ri mediun shade4"></div>
          <div class="circle-ri small shade5"></div>
        </div> 
 </div>
  <div class="clear"></div> 
 <div class="width100 overflow same-padding ow-about-us">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn" data-wow-delay="0.2s" ><?php echo _HEADER_ABOUT_US ?></p>
        <div class="line-startup ow-section-line float-right wow fadeIn" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
 		<div class="text-center three-counter">
        	<img src="img/slogan.png" class="about-png wow fadeIn" data-wow-delay="0.6s" alt="<?php echo _INDEX_MOTTO ?>" title="<?php echo _INDEX_MOTTO ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX_MOTTO ?></p>
            <p class="content-text-p light-blue-text wow fadeIn" data-wow-delay="1s"><?php echo _INDEX_ONCE_READY ?></p>
        </div>
 		<div class="text-center three-counter">
        	<img src="img/target.png" class="about-png wow fadeIn" data-wow-delay="1.2s" alt="<?php echo _INDEX_VISION ?>" title="<?php echo _INDEX_VISION ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="1.4s"><?php echo _INDEX_VISION ?></p>
            <p class="content-text-p light-blue-text wow fadeIn" data-wow-delay="1.6s"><?php echo _INDEX_TO_CREATE_AN ?></p>
        </div>        
  		<div class="text-center three-counter">
        	<img src="img/goal.png" class="about-png wow fadeIn" data-wow-delay="1.8s" alt="<?php echo _INDEX_MISSION ?>" title="<?php echo _INDEX_MISSION ?>">
            <p class="tittle-text-p dark-blue-text wow fadeIn" data-wow-delay="2s"><?php echo _INDEX_MISSION ?></p>
            <p class="content-text-p light-blue-text wow fadeIn" data-wow-delay="2.2s"><?php echo _INDEX_TO_MAX ?></p>
        </div>       
        
 </div>       
   <div class="clear"></div> 
 <div class="width100 overflow same-padding ow-about-us padding-top-bottom">
         
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ow-long-css" data-wow-delay="0.2s" >VinCaps <?php echo _INDEX_INVESTMENT_GOV ?></p>
        <div class="line-startup ow-section-line float-right wow fadeIn ow-long-line" data-wow-delay="0.4s"></div>
        <div class="clear"></div>
 		<div class="text-center two-box-div">
        	<img src="img/cert.png" class="about-png wow fadeIn" data-wow-delay="0.6s">
            
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX_FAR ?></p>
        </div>
 		<div class="text-center two-box-div">
        	<img src="img/prof.png" class="about-png wow fadeIn" data-wow-delay="1s">
            
            <p class="content-text-p dark-blue-text wow fadeIn" data-wow-delay="1.2s"><?php echo _INDEX_SEVEN_PERSON ?></p>
        </div>        
     
        
 </div>       
 <div class="clear"></div>
 <div class="width100 relative overflow"> 
     <div class="context same-padding">
                    <p class="blue-text lato top-des float-left ow-section-des timeline-ow wow fadeIn vx-t" data-wow-delay="0.3s" ><?php echo _INDEX_VINCAPS_PERFOR ?></p>
        <div class="line-startup ow-section-line float-right timeline-line wow fadeIn vx-l" data-wow-delay="0.6s"></div>
        <div class="clear"></div> 
        <p class="per-content-p white-text wow fadeIn" data-wow-delay="0.9s"><?php echo _INDEX_BOOKED ?></p>
        <img src="img/moving-graph.gif" data-wow-delay="1.2s" class="graph-gif  wow fadeIn">
        </div>
    
    
    <div class="area same-padding" >
                <ul class="circles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                </ul>
        </div >
     
    </div> 
 <div class="clear"></div>  
<div class="big-div width100 overflow" id="aboutus">
	<div class="width100 same-padding">

        <div class="startup-width float-left wow fadeIn" data-wow-delay="0.2s">
            <p class="white-text lato top-des"><?php echo _INDEX_YOUR_STARTUP_FUNDING ?></p>
        </div>
        <div class="line-startup float-right wow fadeIn" data-wow-delay="0.4s"></div>
		<div class="clear"></div>
    	<h1 class="lato white-text big-title wow fadeIn" data-wow-delay="0.6s">VinCaps</h1>
        <p class="tav lighter-blue-text description  wow fadeIn" data-wow-delay="0.8s"><?php echo _INDEX_SENTENCE1 ?></p>
        <div class="clear"></div>
		<p class="bottom-text tav white-text wow fadeIn" data-wow-delay="1s"><?php echo _INDEX_PROFESSIONAL_NETWORK ?></p>
       <!--
        <h3 class="white-text lato small-title">Contact Us</h3>
        <p class="contact-p lighter-blue-text tav">
            Contact Number<br>
    		<b class="white-text weight900">016-532 4691 (Kevin Yam)</b>
        </p>
        <p class="contact-p lighter-blue-text tav">
            Email Address<br>
    		<b class="white-text weight900">kevin.vincaps@gmail.com</b>
        </p>  
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp.png" class="social-icon opacity-hover display-inline" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook.png" class="social-icon opacity-hover display-inline" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
        </p>-->
              
    </div>
</div>
<div class="clear"></div>
 <div class="width100 relative overflow"> 
     <div class="context same-padding">
                    <p class="blue-text lato top-des float-left ow-section-des timeline-ow wow fadeIn vx-t" data-wow-delay="0.3s" ><?php echo _INDEX_HOW_DO ?></p>
        <div class="line-startup ow-section-line float-right timeline-line wow fadeIn vx-l" data-wow-delay="0.6s"></div>
        <div class="clear"></div> 
        <p class="per-content-p white-text wow fadeIn" data-wow-delay="0.9s"><?php echo _INDEX_WE_HAVE ?></p>
        <img src="img/proff.png" class="graph-gif proff-png wow pulse" data-wow-duration="15.5s" data-wow-iteration="infinite">
        </div>
    
    
    <div class="area same-padding how-height" >
                <ul class="circles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                </ul>
        </div >
     
    </div> 

<!--<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des ani1 wow fadeIn" data-wow-delay="0.3s">Our Services</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani2" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<h1 class="light-blue-text lato border-text op wow fadeIn ani3" data-wow-delay="0.6s"><b>Our professional team</b> helps you</h1>
            <div class="short-blue-border wow fadeIn ani4" data-wow-delay="0.7s"></div>
            <img src="img/team.jpg" class="border-bottom-img wow fadeIn ani5" data-wow-delay="1s" alt="VinCaps Team" title="VinCaps Team">
        </div>	
        <div class="five-col float-right two-column-css">
        	<table class="list-table">
            	<tbody>
                	<tr class="wow fadeIn ani6" data-wow-delay="0.6s">
                    	<td class="number-td light-blue-text">01</td>
                        <td class="text-td dark-blue-text">Expand your network</td>
                    </tr>
                	<tr class="wow fadeIn ani7" data-wow-delay="0.8s">
                    	<td class="number-td light-blue-text">02</td>
                        <td class="text-td dark-blue-text">Raise funds</td>
                    </tr>                    
                	<tr class="wow fadeIn ani8" data-wow-delay="1s">
                    	<td class="number-td light-blue-text">03</td>
                        <td class="text-td dark-blue-text">Maximize the value of your company</td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
</div>-->

<!--
<div class="clear"></div>  
<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani9" data-wow-delay="0.3s">Our Expertise</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani9" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-right two-column-css">
        	<h1 class="light-blue-text lato border-text op wow fadeIn ani10" data-wow-delay="0.6s"><b>Help you get funded & </b>achieve your business dream</h1>
            <div class="short-blue-border wow fadeIn ani11" data-wow-delay="0.7s"></div>
            <img src="img/achieve.jpg" class="border-bottom-img wow fadeIn ani12" data-wow-delay="1s" alt="Achieve your business dream" title="Achieve your business dream">
        </div>	
        <div class="five-col float-left two-column-css">
        	<table class="list-table">
            	<tbody>
                	<tr class="wow fadeIn ani13" data-wow-delay="0.6s">
                    	<td class="number-td light-blue-text">01</td>
                        <td class="text-td dark-blue-text">Strategic business development and solutions</td>
                    </tr>
                	<tr class="wow fadeIn ani14" data-wow-delay="0.8s">
                    	<td class="number-td light-blue-text">02</td>
                        <td class="text-td dark-blue-text">Investor relationship service</td>
                    </tr>                    
                	<tr class="wow fadeIn ani15" data-wow-delay="1s">
                    	<td class="number-td light-blue-text">03</td>
                        <td class="text-td dark-blue-text">Capital raising exercises</td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
</div>
<div class="clear"></div>
<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des ani1 wow fadeIn" data-wow-delay="0.3s">Our Services</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani2" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<h1 class="light-blue-text lato border-text op wow fadeIn ani3" data-wow-delay="0.6s"><b>Deep into our society. Where
a great startup started.</b></h1>
            <div class="short-blue-border wow fadeIn ani4" data-wow-delay="0.7s"></div>
            <img src="img/team.jpg" class="border-bottom-img wow fadeIn ani5" data-wow-delay="1s" alt="VinCaps Team" title="VinCaps Team">
        </div>	
        <div class="five-col float-right two-column-css">
        	<table class="list-table icon-table-css">
            	<tbody>
                	<tr class="wow fadeIn ani6" data-wow-delay="0.6s">
                    	<td class="vertical-top"><img src="img/vincaps-blog.png" class="table-icon" alt="VinCaps Blog" title="VinCaps Blog"></td>
                        <td class="text-td dark-blue-text"><b>VinCaps Blog</b><br>
                        Tips to raise for your dream business<br>
                        <a href="" class="light-blue-link blog-link">Read Now</a></td>
                    </tr>
                	<tr class="wow fadeIn ani7" data-wow-delay="0.8s">
                    	<td class="vertical-top"><img src="img/insight.png" class="table-icon" alt="Exclusive Insight" title="Exclusive Insight"></td>
                        <td class="text-td dark-blue-text"><b>VinCaps Assessment + Exclusive Insight <img src="img/diamond.png" class="diamond" alt="Premium Member" title="Premium Member"></b><br>
						One click away to know the readiness of your company and exclusive insight for our premium members.<br>
						<a href="" class="orange-link light-blue-link blog-link">Evaluate Now</a></td>
                    </tr>                    
                	<tr class="wow fadeIn ani8" data-wow-delay="1s">
                    	<td class="vertical-top"><img src="img/raise-fund.png" class="table-icon" alt="VinCaps Raise" title="VinCaps Raise"></td>
                        <td class="text-td dark-blue-text"><b>VinCaps Raise</b><br>
                        Connect to our team and investor<br>
                        <a href="#raise" class="light-blue-link blog-link">Contact Us</a></td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
</div>
<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani16" data-wow-delay="0.3s">Network</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani16" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="width100">
        	<h1 class="light-blue-text lato border-text ow-width100 wow fadeIn ani17" data-wow-delay="1s"><b>We connect you to</b></h1>
            <div class="short-blue-border extra-margin-bottom wow fadeIn ani18" data-wow-delay="1.5s"></div>
           
        </div>	
        <div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<table class="list-table icon-table">
            	<tbody>
                	<tr class="wow fadeIn ani19" data-wow-delay="1.8s">
                    	<td class="number-td light-blue-text"><img src="img/equity-crowdfunding.png" class="icon-css" alt="Equity Crowdfunding" title="Equity Crowdfunding"></td>
                        <td class="text-td dark-blue-text">Equity Crowdfunding</td>
                    </tr>
                	<tr class="wow fadeIn ani20" data-wow-delay="2.3s">
                    	<td class="number-td light-blue-text"><img src="img/private-equity.png" class="icon-css" alt="Private Equity" title="Private Equity"></td>
                        <td class="text-td dark-blue-text">Private Equity</td>
                    </tr>                    
                	<tr class="wow fadeIn ani21" data-wow-delay="2.8s">
                    	<td class="number-td light-blue-text"><img src="img/venture-capital.png" class="icon-css" alt="Venture Capital" title="Venture Capital"></td>
                        <td class="text-td dark-blue-text">Venture Capital</td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
        <div class="five-col float-right two-column-css">
        	<table class="list-table icon-table">
            	<tbody>
                	<tr class="wow fadeIn ani22" data-wow-delay="1.8s">
                    	<td class="number-td light-blue-text"><img src="img/family-office.png" class="icon-css" alt="Family Office" title="Family Office"></td>
                        <td class="text-td dark-blue-text">Family Office</td>
                    </tr>
                	<tr class="wow fadeIn ani23" data-wow-delay="2.3s">
                    	<td class="number-td light-blue-text"><img src="img/angel-investor.png" class="icon-css" alt="Angel Investor" title="Angel Investor"></td>
                        <td class="text-td dark-blue-text">Angel Investor</td>
                    </tr>                    
                	<tr class="wow fadeIn ani24" data-wow-delay="2.8s">
                    	<td class="number-td light-blue-text"><img src="img/government-grants.png" class="icon-css" alt="Government Grants" title="Government Grants"></td>
                        <td class="text-td dark-blue-text">Government Grants</td>
                    </tr>                     
                </tbody>
            </table>
        </div>         
</div>
<div class="clear"></div>
<div class="same-padding width100 white-spacing-div overflow" id="testimonial">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani25" data-wow-delay="0.3s">Testimonial</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani25" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<h1 class="light-blue-text lato border-text small-border-text wow fadeIn ani26" data-wow-delay="0.9s"><b>We have helped a start-up get funded from ideation to closing of its funds</b></h1>
            <div class="short-blue-border wow fadeIn ani26" data-wow-delay="1.4s"></div>
            <p class="contact-p light-blue-text tav wow fadeIn ani27" data-wow-delay="1.9s">
                Name of Start-up<br>
                <b class="dark-blue-text weight900">PENTAIP (M) SDN. BHD.[170552-P]</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn ani28" data-wow-delay="2.4s">
                Amount Raised<br>
                <b class="dark-blue-text weight900 big-b">RM1.9 million</b>
                <br><span class="tav dark-blue-text lighter-text">(Despite in RMCO conditions)</span>
        	</p>            
        </div>	
        <div class="five-col float-right two-column-css">
        	<img src="img/funding.jpg" class="width100 funding-img wow fadeIn ani29" data-wow-delay="2.9s">
            <p ><a class="source-p light-blue-text tav light-blue-hover wow fadeIn ani30" data-wow-delay="3.4s" href="https://www.crowdplus.asia/CapitalProfile.php?cid=6789" target="_blank">Source: CrowdPlus.asia</a></p>
        </div> 
</div>-->
<div class="clear"></div>
<div class="same-padding width100 white-spacing-div overflow" id="raise">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani31" data-wow-delay="0.3s">VinCaps <?php echo _INDEX_RAISE ?></p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani31" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <h1 class="price-h1 lato dark-blue-text wow fadeIn ani32" data-wow-delay="0.8s"><img src="img/vincaps-logo.png" class="price-logo" alt="VinCaps" title="VinCaps"> Capital <?php echo _INDEX_PRICE_LIST ?></h1>
        <div class="clear"></div>
        <table class="price-table lato wow fadeIn ani33" data-wow-delay="1.2s">
        	<thead>
                <tr>
                	<th class="th4 title-td"></th>
                    <th class="th4 title-td"><?php echo _INDEX_PRICE ?></th>
                </tr>
            </thead>
        	<tbody>
                <tr>
                	<td class="td1 content-td"><?php echo _INDEX_COMPANY_A2 ?></td>
                    <td class="td1 content-td2">RM12,000</td>
                </tr>
<!--                <tr>
                	<td class="td2 content-td"><?php echo _INDEX_PITCH_DECK_PRE ?></td>
                    <td class="td2 content-td2">RM1,000</td>
                </tr> 
                <tr>
                	<td class="td1 content-td"><?php echo _INDEX_PRESENT_1_ON_1 ?></td>
                    <td class="td1 content-td2">RM1,250</td>
                    
                </tr>
                <tr>
                	<td class="td2 content-td"><?php echo _INDEX_COMPANY_PROFILE ?></td>
                    <td class="td2 content-td2">RM950</td>
                    
                    
                </tr>   
                <tr>
                	<td class="td1 content-td"><?php echo _INDEX_CORPORATE_BRANDING ?></td>
                    <td class="td1 content-td2">RM1,800</td>
                </tr>  -->                  
                <tr>
                	<td class="td2 content-td"><?php echo _INDEX_FACT_SHEET ?></td>
                    <td class="td2 content-td2">RM1,000</td>
                </tr>                
<!--                <tr>
                	<td class="td1 content-td">Professional Business Plan</td>
                    <td class="td1 content-td2">RM500</td>
                    <td class="td1 content-td2">RM1,000</td>
                    
                </tr>-->
                <tr>
                	<td class="td1 content-td"><?php echo _INDEX_CONSULTANCY_SER ?></td>
                    <td class="td1 content-td2">RM150 <?php echo _INDEX_PER_HOUR ?></td>
                </tr>                  
                                                                                
            </tbody>
        </table>
        <p class="ps wow fadeIn ani33" data-wow-delay="1.5s"><?php echo _INDEX_OPTIONAL ?></p>
</div>  
<div class="clear"></div>       

  

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani31" data-wow-delay="0.3s"><?php echo _HEADER_CONTACT_US ?></p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani31" data-wow-delay="0.5s"></div>
		    
        <div class="clear"></div>       
        
<div class="five-col float-left two-column-css">
  <form class="form-class extra-margin" action="utilities/submitContactUsFunction.php" method="POST">
    <input type="text" name="name" placeholder="<?php echo _INDEX_NAME ?>" class="input-name clean dark-blue-text wow fadeIn ani32" data-wow-delay="0.8s" required >
    <input type="email" name="email" placeholder="<?php echo _INDEX_EMAIL ?>" class="input-name clean lato dark-blue-text wow fadeIn ani33" data-wow-delay="1.4s" required >
    <input type="text" name="telephone" placeholder="<?php echo _INDEX_CONTACT_NUMBER ?>" class="input-name clean lato dark-blue-text wow fadeIn ani34" data-wow-delay="2.0s" required >
    
    <select  class="input-name clean lato dark-blue-text wow fadeIn ani34" data-wow-delay="2.2s" id="selection" name="selection" required>
      <option value='Investor'>I would like to be an investor to back projects</option>
      <option value='Business Owner'>I am a business owner seeking for funding</option>
    </select> 

    <textarea name="comments" placeholder="<?php echo _INDEX_MESSAGE ?>" class="input-name input-message clean lato dark-blue-text wow fadeIn ani35" data-wow-delay="2.6s" ></textarea>
    <div class="clear"></div>
    <table class="form-table">
      <tbody>
        <tr class="wow fadeIn ani36" data-wow-delay="3.2s">
          <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_UPDATED_WITH ?></p></td>
        </tr>
        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
          <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
          <td><p class="opt-msg lato dark-blue-text"><?php echo _INDEX_I_JUST_WANT ?></p></td>
        </tr>
      </tbody>
    </table>
    <div class="res-div">
      <input type="submit" name="send_email_button" value="<?php echo _INDEX_SEND ?>" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="4.4s">
    </div>
  </form>
</div>	

        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text tav wow fadeIn ani39" data-wow-delay="4.5s">
                <?php echo _INDEX_COMPANY ?><br>
                <b class="dark-blue-text weight900">Vincaps Capital Sdn. Bhd.</b>
        	</p>         
			<p class="contact-p light-blue-text tav wow fadeIn ani39" data-wow-delay="4.9s">
                <?php echo _INDEX_CONTACT_NUMBER ?><br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60163324691">+6016 332 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn ani40" data-wow-delay="5.4s">
                <?php echo _INDEX_EMAIL ?><br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text tav wow fadeIn ani41" data-wow-delay="5.9s">
                <?php echo _INDEX_ADDRESS ?><br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>
            <p class="contact-p light-blue-text tav wow fadeIn ani39" data-wow-delay="6.4s">
                <?php echo _INDEX_COMPANY_NO ?><br>
                <b class="dark-blue-text weight900">202001034287(1390608-M)</b>
        	</p>                
            
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="6.4s" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="6.9s" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="7.4s" alt="VinCaps Instagram" title="VinCaps Instagram"></a>
        </p>  
        <p class="contact-p lighter-blue-text tav wow fadeIn ani42" data-wow-delay="7.6s"><a href="team.php" target="_blank" class=" blue-link"><?php echo _INDEX_CHECK_OUT_VIN ?></a></p>                      
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn ani42" data-wow-delay="7.8s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<?php include 'js.php'; ?>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via VinCaps website";

    function died($error)
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }


    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }



    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments'];
	$selection = $_POST['selection'];
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;

	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }


    $string_exp = "/^[A-Za-z .'-]+$/";

  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }




  if(strlen($error_message) > 0) {
    died($error_message);
  }

    $email_message = "Form details below.\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }

    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Status : ".clean_string($selection)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
echo '<script>alert("Thank for messaging us. You can visit our Facebook page for more details while waiting for the reply.")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php

}
?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>