<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/event.php" />
<link rel="canonical" href="https://vincaps.com/event.php" />
<meta property="og:title" content="Teh Tarik Session | VinCaps" />
<title>Teh Tarik Session | VinCaps</title>
<meta property="og:description" content="Unlock your financial freedom through talking over a cup of ‘teh tarik’ with Mr Kevin and getting the secret of accessing new funding" />
<meta name="description" content="Unlock your financial freedom through talking over a cup of ‘teh tarik’ with Mr Kevin and getting the secret of accessing new funding" />
<meta name="keywords" content="teh tarik, vincaps, kevin, Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow dark-blue-bg teh1-div same-padding">
	<div class="left-teh1">
    	<img src="img/milk.png" data-wow-delay="0.2s" class="milk wow fadeIn">
        <h1 class="white-text teh-h1 lato wow fadeIn" data-wow-delay="0.4s">Teh Tarik Session<img src="img/milktea.png" class="milktea wow fadeIn" data-wow-delay="0.6s" alt="Teh Tarik" title="Teh Tarik"></h1>
        <div class="teh-border white-border wow fadeIn" data-wow-delay="0.8s"></div>
        <p class="teh-content-p white-text wow fadeIn" data-wow-delay="1s">Unlock your financial freedom through talking over a cup of ‘teh tarik’ with Mr Kevin and getting the secret of accessing new funding</p>
        <table class="teh-info-table lato">
        	<tr class=" wow fadeIn" data-wow-delay="1.2s">
            	<td>Date</td>
                <td>:</td>
                <td><b>2nd May (Sunday)</b></td>
            </tr>
        	<tr class=" wow fadeIn" data-wow-delay="1.4s">
            	<td>Time</td>
                <td>:</td>
                <td><b>9pm - 10pm</b></td>
            </tr> 
        	<tr class=" wow fadeIn" data-wow-delay="1.6s">
            	<td>Venue</td>
                <td>:</td>
                <td><b>Zoom and Facebook Live</b></td>
            </tr>                          
        </table>
    </div>
	<div class="right-teh1 wow fadeIn" data-wow-delay="1.8s">
    	<img src="img/teh-tarik.png" class="width100" alt="Teh Tarik Session" title="Teh Tarik Session">
    </div>    
</div>
<div class="clear"></div> 
<div class="width100 overflow text-center teh2-div same-padding text-center padding-top-bottom2">
	<h1 class="teh-all-h1 dark-blue-text wow fadeIn" data-wow-delay="0.2s">Dream of Us</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc light-blue-text wow fadeIn" data-wow-delay="0.6s">We know most of you are <b>aspiring entrepreneurs</b> and in the journey to </p>
    <div class="four-column-big-div">
    	<div class="four-col">
        	<img src="img/prof.png" class="wow fadeIn" data-wow-delay="0.8s" alt="Inspired Ideal Entrepreneur" title="Inspired Ideal Entrepreneur">
            <p class="wow fadeIn" data-wow-delay="1s">Become the <b>inspired ideal entrepreneur</b></p>
        </div>
  
    	<div class="four-col">
        	<img src="img/goal.png" class="wow fadeIn" data-wow-delay="1.2s" alt="Grow your business exponentially" title="Grow your business exponentially">
            <p class="wow fadeIn" data-wow-delay="1.4s"><b>Grow your business exponentially</b></p>
        </div>
   
    	<div class="four-col">
        	<img src="img/slogan.png" class="wow fadeIn" data-wow-delay="1.6s" alt="Achieve financial freedom" title="Achieve financial freedom">
            <p class="wow fadeIn" data-wow-delay="1.8s">Achieve <b>financial freedom</b></p>
        </div>
   
    	<div class="four-col">
        	<img src="img/successful.png" class="wow fadeIn" data-wow-delay="2s" alt="Successful Businessman" title="Successful Businessman">
            <p  class="wow fadeIn" data-wow-delay="2.2s">Become a  <b>successful businessman</b></p>
        </div>
    </div>    
</div>
<div class="clear"></div> 
<div class="width100 overflow text-center teh2-div same-padding text-center padding-top-bottom2 dark-blue-bg">
	<h1 class="teh-all-h1 white-text wow fadeIn" data-wow-delay="0.2s">Pain Point for Entrepreneur</h1>
	<div class="teh-border white-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc white-text wow fadeIn" data-wow-delay="0.6s">In the journey of entrepreneurship , you might facing some obstacles such as</p>
    <div class="ow-question">
 		<div class="text-center three-column wow fadeIn" data-wow-delay="0.8s">
        	
            <p>Feeling uncomfortable asking friends or family for business loans?</p>
        </div>
 		<div class="text-center three-column mid-three-column second-three-column wow fadeIn" data-wow-delay="1s">
        	
            <p>Your bank loan application gets rejected?</p>
        </div>        
  		<div class="text-center three-column wow fadeIn" data-wow-delay="1.2s">
        	
            <p>Having insufficient money to kick start your business?</p>
        </div> 
 		<div class="text-center three-column second-three-column wow fadeIn" data-wow-delay="1.4s">
        	
            <p>When you’re pitching your business to an investor, you receive a big “NO” from them?</p>
        </div>
 		<div class="text-center three-column mid-three-column wow fadeIn" data-wow-delay="1.6s">
        	
            <p>Or you even can’t find any investors for your business?</p>
        </div>        
  		<div class="text-center three-column second-three-column wow fadeIn" data-wow-delay="1.8s">
        	
            <p>Can you really pay your bills in the long term?</p>
        </div>         
     </div>   
</div>
<div class="clear"></div> 
 
<div class="width100 overflow text-center teh2-div same-padding text-center padding-top-bottom2">
	<h1 class="teh-all-h1 dark-blue-text wow fadeIn" data-wow-delay="0.2s">Did You Know?</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc light-blue-text wow fadeIn res-width" data-wow-delay="0.6s">Every year 1 in 12 businesses closes and only 4 out of 100 businesses survive past the 10-year mark. Most of businesses fail because they <b>can’t pay their bills.</b></p>
</div>
 <div class="clear"></div> 
 <img src="img/teh-tarik-banner.jpg" class="width100 wow fadeIn" data-wow-delay="0.8s">
 <div class="clear"></div> 
 
<div class="width100 overflow text-center teh2-div same-padding text-center padding-top-bottom2">
	<h1 class="teh-all-h1 dark-blue-text wow fadeIn" data-wow-delay="0.2s">Teh Tarik Session is Here to Help You</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc dark-blue-text wow fadeIn res-width jus-cen" data-wow-delay="0.6s">Our mission is to help you to accelerate and scale your business to the next level by “pulling” you to embark the journey of financial freedom with our specialty ‘teh tarik’.<br><br>

Such specialty is designed to share the secrets of accessing new funding, in which consist of all the ingredients that you need when looking for funding as well as the methods and requirements. <br><br>

We all know that having sufficient money is important in running a business or even in our life. Are you currently keep taking your own money for investment? Instead of personal investment, would it be better to make others take their money to invest in you?<br><br>

Want to know more secret, join <a href="#register1" class="dark-blue-text"><b class="pointer opacity-hover dark-blue-text">Teh Tarik Session</b></a> to unlock it</p>
</div>
<div class="clear"></div> 
<div class="width100 overflow text-center teh2-div same-padding text-center padding-top-bottom2">
	<h1 class="teh-all-h1 dark-blue-text wow fadeIn" data-wow-delay="0.2s">Topics Covered in Our Course</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    
    <div class="ow-no-question">
 		<div class="text-center three-column">
        	<img src="img/shake-hands.png" class=" wow fadeIn" data-wow-delay="0.6s" alt="Benefit of external funding" title="Benefit of external funding">
            <p class=" wow fadeIn" data-wow-delay="0.8s">Benefit of external funding</p>
        </div>
 		<div class="text-center three-column mid-three-column second-three-column">
        	<img src="img/private-equity.png" class="wow fadeIn" data-wow-delay="1s" alt="Way to get funding" title="Way to get funding">
            <p class=" wow fadeIn" data-wow-delay="1.2s">Way to get funding</p>
        </div>        
  		<div class="text-center three-column">
        	<img src="img/edit-profile.png" class="wow fadeIn" data-wow-delay="1.4s" alt="Requirement to get funding" title="Requirement to get funding">
            <p class=" wow fadeIn" data-wow-delay="1.6s">Requirement to get funding</p>
        </div> 
 		<div class="text-center three-column second-three-column">
        	<img src="img/raise-fund.png" class="wow fadeIn" data-wow-delay="1.8s" alt="Process of funding" title="Process of funding">
            <p class=" wow fadeIn" data-wow-delay="2s">Process of funding</p>
        </div>
 		<div class="text-center three-column mid-three-column">
        	<img src="img/presentation.png" class="wow fadeIn" data-wow-delay="2.2s" alt="Key ingredient when pitching" title="Key ingredient when pitching">
            <p class=" wow fadeIn" data-wow-delay="2.4s">Key ingredient when pitching</p>
        </div>        
<!--  		<div class="text-center three-column second-three-column">
        	<img src="img/profile.png" class="wow fadeIn" data-wow-delay="2.6s" alt="Company profile" title="Company profile">
            <p  class=" wow fadeIn" data-wow-delay="2.8s">Company profile</p>
        </div>  -->       
     </div>   
</div>
<div class="clear"></div> 
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2 dark-blue-bg">
	<h1 class="teh-all-h1 text-center white-text wow fadeIn" data-wow-delay="0.2s">Our Speaker</h1>
	<div class="teh-border white-border margin-auto wow fadeIn border-margin" data-wow-delay="0.4s"></div>
    <div class="left-img-div1">
    	<img src="img/kevin.png" class="width100 wow fadeIn" data-wow-delay="0.6s" alt="Mr.Kevin Yam" title="Mr.Kevin Yam">
    </div>
    <div class="right-content-div1">
    	<h1 class="white-text wow fadeIn" data-wow-delay="0.8s">Mr. Kevin Yam</h1>
    	<p class="white-text wow fadeIn" data-wow-delay="1s">Our expert in-house as Entrepreneur-in-Residence, Mr Kevin Yam, the Managing Partner of VinCaps Capital. With his passion and well experience in the funding industry, he has tailored funding campaigns that have helped raised close to RM6 million within a quarter with another incoming RM35 million in within the next 2 quarters.</p>
    </div>
    
</div>
<div class="clear"></div> 
<div class="width100 overflow teh2-div same-padding padding-top-bottom2">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Key Takeaways</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc light-blue-text wow fadeIn res-width last-cen" data-wow-delay="0.6s">Learn from our experienced expert, Mr Kevin who is in the relevant fields for many years and get his secret recipe of funding to create insane growth in your business, in only one hour of a single weekend!</p>
    <div class="four-column-big-div">
    	<div class="four-col text-center">
        	<img src="img/insight.png" class="wow fadeIn" data-wow-delay="0.8s" alt="Readiness of your company" title="Readiness of your company">
            <p class="wow fadeIn" data-wow-delay="1s">Knowing the <b>readiness of your company</b></p>
        </div>
  
    	<div class="four-col text-center">
        	<img src="img/equity-crowdfunding.png" class="wow fadeIn" data-wow-delay="1.2s" alt="Latest funding techniques" title="Latest funding techniques">
            <p class="wow fadeIn" data-wow-delay="1.4s">Latest <b>funding techniques</b></p>
        </div>
   
    	<div class="four-col text-center">
        	<img src="img/target.png" class="wow fadeIn" data-wow-delay="1.6s" alt="Essential details regarding funding" title="Essential details regarding funding">
            <p class="wow fadeIn" data-wow-delay="1.8s"><b>Essential details</b> regarding funding</p>
        </div>
   
    	<div class="four-col text-center">
        	<img src="img/job.png" class="wow fadeIn" data-wow-delay="2s" alt="Professional guidance" title="Professional guidance">
            <p  class="wow fadeIn" data-wow-delay="2.2s"><b>Professional guidance</b> on getting funded </p>
        </div>
     	<div class="four-col text-center">
        	<img src="img/global.png" class="wow fadeIn" data-wow-delay="2.4s" alt="Next level of business growth" title="Next level of business growth">
            <p class="wow fadeIn" data-wow-delay="2.6s">Next level of <b>business growth</b></p>
        </div>
   
    	<div class="four-col text-center">
        	<img src="img/premium-member.png" class="wow fadeIn" data-wow-delay="2.8s" alt="Other additional value" title="Other additional value">
            <p class="wow fadeIn" data-wow-delay="3s"><b>Other additional value</b></p>
        </div>
   
    	<div class="four-col text-center">
        	<img src="img/present.png" class="wow fadeIn" data-wow-delay="3.2s" alt="Secret prize" title="Secret prize">
            <p  class="wow fadeIn" data-wow-delay="3.4s"><b>Secret prize</b> at the end of the webinar</p>
        </div>       
        
        
        
    </div>    
</div>

<div class="clear"></div>       
<!--<div class="width100 overflow teh2-div same-padding  padding-top-bottom2">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Testimonials</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn border-margin" data-wow-delay="0.4s"></div>
    <div class="left-img-div1 p-div">
    	<img src="img/black.png" class="p-width wow fadeIn" data-wow-delay="0.6s" alt="PENTAIP" title="PENTAIP">
    </div>
    <div class="right-content-div1">
    	
    	<p class="dark-blue-text wow fadeIn" data-wow-delay="0.8s">Company that we helped to get funded
PENTAIP, is a technology company that is currently developing its own Big Data based Artificial Intelligence (AI) Wealth Management Technology. We recommended Pentaip raise funds to one of the regulated ECF platforms, position the founder as the persona of the company, get them ready for fundraising and help them approach investors. As of 13rd July 2020, the company has raised RM1.13million from our recommended funding method. 
</p>
    </div>
    
</div>
<div class="clear"></div> -->
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2 dark-blue-bg">
	<h1 class="teh-all-h1 white-text text-center wow fadeIn" data-wow-delay="0.2s">Countdown for the Webinar </h1>
	<div class="teh-border white-border margin-auto wow fadeIn" data-wow-delay="0.4s"></div>
    <div class="clear"></div>
    <ul class="countdown wow fadeIn" data-wow-delay="0.6s">
        <li>
            <span class="days">00</span>
            <p class="days_ref">days</p>
        </li>
        <li class="seperator">.</li>
        <li>
            <span class="hours">00</span>
            <p class="hours_ref">hours</p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="minutes">00</span>
            <p class="minutes_ref">minutes</p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="seconds">00</span>
            <p class="seconds_ref">seconds</p>
        </li>
    </ul>  
</div>
<div class="clear"></div> 
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Register Now</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn border-margin" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc light-blue-text wow fadeIn res-width last-cen" data-wow-delay="0.6s">Get yourself one hour, to learn the secret of accessing new funding and bring your business to the next level!</p>      
        <div class="five-col float-left two-column-css">

        <form method="POST" action="utilities/webinarRegistrationFunction.php">
            <input type="text" placeholder="Name" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" data-wow-delay="0.8s" required >
            <input type="email" placeholder="Email" id="email" name="email" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1s" required >
            <input type="text" type="text" placeholder="Contact No." id="phone" name="phone" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.2s" required >
            <div class="res-div"><input type="submit" name="submit" value="Register" class="input-submit blue-button white-text clean pointer lato wow fadeIn" data-wow-delay="1.4s"></div>
        </form>
            
        </div>	
        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text tav wow fadeIn" data-wow-delay="1.6s" style="margin-top:0 !important;">
                For inquiries, may approach us on <br>Contact Number<br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60165324691">+6016 532 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn" data-wow-delay="1.8s">
                Email Address<br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text tav wow fadeIn" data-wow-delay="2s">
                Address<br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>            
            
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="2.2s" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="2.4s" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="2.6s" alt="VinCaps Instagram" title="VinCaps Instagram"></a>
        </p>  
        <p class="contact-p lighter-blue-text tav wow fadeIn" data-wow-delay="2.8s"><a href="team.php" target="_blank" class=" blue-link">Check Out VinCaps Corporate Structure & Internal Team</a></p>                      
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn" data-wow-delay="3s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>


</body>
</html>