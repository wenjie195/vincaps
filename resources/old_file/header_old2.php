<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype'] == 0)
    //admin
    {
    ?>
        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
<!--                      <div class="dropdown" style="display:inline-block !important;">
                                    <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                Membership <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="adminMembershipPending.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Pending</a></p>
                                        <p class="dropdown-p"><a href="adminMembershipApproved.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Approved</a></p>
                                        <p class="dropdown-p"><a href="adminMembershipRejected.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Rejected</a></p>
										<p class="dropdown-p"><a href="adminMember.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">All Members</a></p>
                                    </div>
                    </div> -->
<!--                      <div class="dropdown" style="display:inline-block !important;">
                                    <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                Blog <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="adminBlogView.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">View</a></p>
                                        <p class="dropdown-p"><a href="adminBlogAdd.php"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">Add</a></p>
                                       
										
                                    </div>
                    </div>   -->

                        <a href="adminContactUsView.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                           Contact Us
                        </a>  

                        <a href="adminRegistrationWebinar.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                           <?php echo _MENU_WEBINAR_REG ?>
                        </a>                    
                        <a href="adminRegistrationPending.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _MENU_WEBINAR ?>
                        </a>  
                        <a href="adminBlogView.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _MENU_VIEW ?>
                        </a>      
                         <a href="adminBlogAdd.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _MENU_ADD_ARTICLE ?>
                        </a>                                           
                        <a href="logout.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            <?php echo _MENU_LOGOUT ?>
                        </a>  
                            	
                    </div>
                </div>
        </header>
    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    //user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                        <a href="index.php#aboutus" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_ABOUT_US ?>
                        </a>             
                    <div class="dropdown">
                                    <a  class="menu-margin-right menu-item lato">
                                                <?php echo _MENU_PROFILE ?> <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PASSWORD ?></a></p>

                                    </div>
                    </div>                
                        <a href="fundraisingBlog.php" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_BLOG ?>
                        </a>
                        <a href="logout.php" class="menu-margin-right menu-item lato">
                            <?php echo _MENU_LOGOUT ?>
                        </a>  
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger"><?php echo _HEADER_OPEN_MENU ?></button>
                            <ul class="dl-menu">
                                <li><a href="index.php"><?php echo _HEADER_ABOUT_US ?></a></li>
                                <li><a href="profile.php"><?php echo _MENU_PROFILE ?></a></li>    
                                <li><a href="editProfile.php"><?php echo _MENU_EDIT_PROFILE ?></a></li> 
                                <li><a href="editPassword.php"><?php echo _MENU_EDIT_PASSWORD ?></a></li>                             
                                <li><a href="fundraisingBlog.php"><?php echo _HEADER_BLOG ?></a></li>                               
                                <li><a href="logout.php"><?php echo _MENU_LOGOUT ?></a></li>
                            </ul>
                        </div>                	
                    </div>
                </div>
        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 2)
    //premium user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                        <a href="index.php" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_ABOUT_US ?>
                        </a>   
                        
                     
                    <div class="dropdown">
                                    <a  class="menu-margin-right menu-item lato">
                                                <?php echo _MENU_PROFILE ?> <img src="img/dropdown.png" class="dropdown-png">

                                    </a>
                                    <div class="dropdown-content yellow-dropdown-content">
                                    	<p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PROFILE ?></a></p>
                                        <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a menu-item"><?php echo _MENU_EDIT_PASSWORD ?></a></p>

                                    </div>
                    </div>                        
             
                        <a href="fundraisingBlog.php" class="menu-margin-right menu-item lato">
                            <?php echo _HEADER_BLOG ?>
                        </a>
                        <a href="logout.php" class="menu-margin-right menu-item lato">
                            <?php echo _MENU_LOGOUT ?>
                        </a>  
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger"><?php echo _HEADER_OPEN_MENU ?></button>
                            <ul class="dl-menu">
                                <li><a href="index.php#aboutus"><?php echo _HEADER_ABOUT_US ?></a></li>
                                <li><a href="profile.php"><?php echo _MENU_PROFILE ?></a></li>    
                                <li><a href="editProfile.php"><?php echo _MENU_EDIT_PROFILE ?></a></li> 
                                <li><a href="editPassword.php"><?php echo _MENU_EDIT_PASSWORD ?></a></li>                             
                                <li><a href="fundraisingBlog.php"><?php echo _HEADER_BLOG ?></a></li>                             
                                <li><a href="logout.php"><?php echo _MENU_LOGOUT ?></a></li>
                            </ul>
                        </div>                	
                    </div>
                </div>
        </header>

    <?php
    }
    ?>

<?php
}
else
{
?>
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"><img src="img/vincaps-logo.png" class="logo-img opacity-hover" alt="VinCaps" title="VinCaps"></a>
                </div>

                <div class="right-menu-div float-right before-header-div">

                    <a href="index.php" class="menu-margin-right menu-item lato">
                        <?php echo _HEADER_ABOUT_US ?>
                    </a>      

                    
 
                    <a href="index.php#contactus" class="menu-margin-right menu-item lato">
                        <?php echo _HEADER_CONTACT_US ?>
                    </a>
                    <a href="team.php" class="menu-margin-right menu-item lato">
                        <?php echo _HEADER_TEAM ?>
                    </a>                                  
                    <a href="fundraisingBlog.php" class="menu-margin-right menu-item lato">
                        <?php echo _HEADER_BLOG ?>
                    </a>
                    <div class="dropdown menu-item langz" >
                                                        <a  class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                                                                   EN/中文 <img src="img/dropdown.png" class="dropdown-png">
                    
                                                        </a>
                                                        <div class="dropdown-content yellow-dropdown-content">
                                                            <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">English Version</a></p>
                                                            <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a menu-item" style="display:inline-block !important;">中文版本</a></p>
                                                            
                                                        </div>
                                        </div>        

                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                            <li><a href="index.php"><?php echo _HEADER_ABOUT_US ?></a></li>
                             <li><a href="index.php#contactus"><?php echo _HEADER_CONTACT_US ?></a></li>
                            <li><a href="team.php"><?php echo _HEADER_TEAM ?></a></li>                    
                            <li><a href="fundraisingBlog.php"><?php echo _HEADER_BLOG ?></a></li>
                            <li class=" langz"><a href="<?php $link ?>?lang=ch">English Version</a></li>
                            <li class=" langz"><a href="<?php $link ?>?lang=en">中文版本</a></li>                            
                            <!--<li><a href="event.php">Event</a></li>-->
<!--                            <li><a class="open-signup">Register</a></li>                                
                            <li><a class="open-login">Login</a></li>-->
                            </ul>
                    </div>
                                            
                </div>
            </div>
    </header>
<?php
}
?>