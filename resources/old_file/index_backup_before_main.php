<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="VinCaps | Your Startup Funding Consultant" />
<title>VinCaps | Your Startup Funding Consultant</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="big-div width100 overflow" id="aboutus">
	<div class="width100 same-padding">

        <div class="startup-width float-left wow fadeIn" data-wow-delay="0.2s">
            <p class="white-text lato top-des">Your Startup Funding Consultant</p>
        </div>
        <div class="line-startup float-right wow fadeIn" data-wow-delay="0.4s"></div>
		<div class="clear"></div>
    	<h1 class="lato white-text big-title wow fadeIn" data-wow-delay="0.6s">VinCaps</h1>
        <p class="tav lighter-blue-text description  wow fadeIn" data-wow-delay="0.8s">We are helping SMEs and Start-ups get funded within 90 days!</p>
        <div class="clear"></div>
		<p class="bottom-text tav white-text wow fadeIn" data-wow-delay="1s">Professional Network As a Service Platform</p>
       <!--
        <h3 class="white-text lato small-title">Contact Us</h3>
        <p class="contact-p lighter-blue-text tav">
            Contact Number<br>
    		<b class="white-text weight900">016-532 4691 (Kevin Yam)</b>
        </p>
        <p class="contact-p lighter-blue-text tav">
            Email Address<br>
    		<b class="white-text weight900">kevin.vincaps@gmail.com</b>
        </p>  
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp.png" class="social-icon opacity-hover display-inline" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook.png" class="social-icon opacity-hover display-inline" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
        </p>-->
              
    </div>
</div>
<div class="clear"></div>
<!--<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des ani1 wow fadeIn" data-wow-delay="0.3s">Our Services</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani2" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<h1 class="light-blue-text lato border-text op wow fadeIn ani3" data-wow-delay="0.6s"><b>Our professional team</b> helps you</h1>
            <div class="short-blue-border wow fadeIn ani4" data-wow-delay="0.7s"></div>
            <img src="img/team.jpg" class="border-bottom-img wow fadeIn ani5" data-wow-delay="1s" alt="VinCaps Team" title="VinCaps Team">
        </div>	
        <div class="five-col float-right two-column-css">
        	<table class="list-table">
            	<tbody>
                	<tr class="wow fadeIn ani6" data-wow-delay="0.6s">
                    	<td class="number-td light-blue-text">01</td>
                        <td class="text-td dark-blue-text">Expand your network</td>
                    </tr>
                	<tr class="wow fadeIn ani7" data-wow-delay="0.8s">
                    	<td class="number-td light-blue-text">02</td>
                        <td class="text-td dark-blue-text">Raise funds</td>
                    </tr>                    
                	<tr class="wow fadeIn ani8" data-wow-delay="1s">
                    	<td class="number-td light-blue-text">03</td>
                        <td class="text-td dark-blue-text">Maximize the value of your company</td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
</div>-->

<div class="same-padding width100 white-spacing-div overflow" id="app">

    <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani31" data-wow-delay="1.3s">VinCaps Blog</p>

	<div class="clear"></div>
        <?php
        $conn = connDB();
        if($articles)
        {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
        <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
            <div class="article-card article-card-overwrite opacity-hover wow fadeIn ani31" data-wow-delay="1.5s">
           
            <div class="article-bg-img-box progressive">
            	
  					<img data-src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" src="img/preload.jpg" class="preview width100 lazy" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>"/>
				</a>
           </div>           
           <div class="box-caption box2">
            <div class="wrap-a wrap100">
            <span class="light-blue-text small-date"><a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="light-blue-text small-date"><?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?></span>
            </div>
 <a href='blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a dark-blue-text">
                                    <?php echo $articles[$cnt]->getTitle();?>
                                </div>

                                <div class="text-content-div dark-blue-text">
                                    <?php echo $description = $articles[$cnt]->getKeywordOne();?>

                                </div>
								</a>
                            </div>
                            
                        </div>
                    </a>    
        <?php
        }
        ?>
        <?php
        }
        $conn->close();
        ?>


</div>  

<div class="clear"></div>  
<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani9" data-wow-delay="0.3s">Our Expertise</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani9" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-right two-column-css">
        	<h1 class="light-blue-text lato border-text op wow fadeIn ani10" data-wow-delay="0.6s"><b>Help you get funded & </b>achieve your business dream</h1>
            <div class="short-blue-border wow fadeIn ani11" data-wow-delay="0.7s"></div>
            <img src="img/achieve.jpg" class="border-bottom-img wow fadeIn ani12" data-wow-delay="1s" alt="Achieve your business dream" title="Achieve your business dream">
        </div>	
        <div class="five-col float-left two-column-css">
        	<table class="list-table">
            	<tbody>
                	<tr class="wow fadeIn ani13" data-wow-delay="0.6s">
                    	<td class="number-td light-blue-text">01</td>
                        <td class="text-td dark-blue-text">Strategic business development and solutions</td>
                    </tr>
                	<tr class="wow fadeIn ani14" data-wow-delay="0.8s">
                    	<td class="number-td light-blue-text">02</td>
                        <td class="text-td dark-blue-text">Investor relationship service</td>
                    </tr>                    
                	<tr class="wow fadeIn ani15" data-wow-delay="1s">
                    	<td class="number-td light-blue-text">03</td>
                        <td class="text-td dark-blue-text">Capital raising exercises</td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
</div>
<div class="clear"></div>
<!--<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des ani1 wow fadeIn" data-wow-delay="0.3s">Our Services</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani2" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<h1 class="light-blue-text lato border-text op wow fadeIn ani3" data-wow-delay="0.6s"><b>Deep into our society. Where
a great startup started.</b></h1>
            <div class="short-blue-border wow fadeIn ani4" data-wow-delay="0.7s"></div>
            <img src="img/team.jpg" class="border-bottom-img wow fadeIn ani5" data-wow-delay="1s" alt="VinCaps Team" title="VinCaps Team">
        </div>	
        <div class="five-col float-right two-column-css">
        	<table class="list-table icon-table-css">
            	<tbody>
                	<tr class="wow fadeIn ani6" data-wow-delay="0.6s">
                    	<td class="vertical-top"><img src="img/vincaps-blog.png" class="table-icon" alt="VinCaps Blog" title="VinCaps Blog"></td>
                        <td class="text-td dark-blue-text"><b>VinCaps Blog</b><br>
                        Tips to raise for your dream business<br>
                        <a href="" class="light-blue-link blog-link">Read Now</a></td>
                    </tr>
                	<tr class="wow fadeIn ani7" data-wow-delay="0.8s">
                    	<td class="vertical-top"><img src="img/insight.png" class="table-icon" alt="Exclusive Insight" title="Exclusive Insight"></td>
                        <td class="text-td dark-blue-text"><b>VinCaps Assessment + Exclusive Insight <img src="img/diamond.png" class="diamond" alt="Premium Member" title="Premium Member"></b><br>
						One click away to know the readiness of your company and exclusive insight for our premium members.<br>
						<a href="" class="orange-link light-blue-link blog-link">Evaluate Now</a></td>
                    </tr>                    
                	<tr class="wow fadeIn ani8" data-wow-delay="1s">
                    	<td class="vertical-top"><img src="img/raise-fund.png" class="table-icon" alt="VinCaps Raise" title="VinCaps Raise"></td>
                        <td class="text-td dark-blue-text"><b>VinCaps Raise</b><br>
                        Connect to our team and investor<br>
                        <a href="#raise" class="light-blue-link blog-link">Contact Us</a></td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
</div>-->
<div class="same-padding width100 white-spacing-div overflow">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani16" data-wow-delay="0.3s">Network</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani16" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="width100">
        	<h1 class="light-blue-text lato border-text ow-width100 wow fadeIn ani17" data-wow-delay="1s"><b>We connect you to</b></h1>
            <div class="short-blue-border extra-margin-bottom wow fadeIn ani18" data-wow-delay="1.5s"></div>
           
        </div>	
        <div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<table class="list-table icon-table">
            	<tbody>
                	<tr class="wow fadeIn ani19" data-wow-delay="1.8s">
                    	<td class="number-td light-blue-text"><img src="img/equity-crowdfunding.png" class="icon-css" alt="Equity Crowdfunding" title="Equity Crowdfunding"></td>
                        <td class="text-td dark-blue-text">Equity Crowdfunding</td>
                    </tr>
                	<tr class="wow fadeIn ani20" data-wow-delay="2.3s">
                    	<td class="number-td light-blue-text"><img src="img/private-equity.png" class="icon-css" alt="Private Equity" title="Private Equity"></td>
                        <td class="text-td dark-blue-text">Private Equity</td>
                    </tr>                    
                	<tr class="wow fadeIn ani21" data-wow-delay="2.8s">
                    	<td class="number-td light-blue-text"><img src="img/venture-capital.png" class="icon-css" alt="Venture Capital" title="Venture Capital"></td>
                        <td class="text-td dark-blue-text">Venture Capital</td>
                    </tr>                     
                </tbody>
            </table>
        </div> 
        <div class="five-col float-right two-column-css">
        	<table class="list-table icon-table">
            	<tbody>
                	<tr class="wow fadeIn ani22" data-wow-delay="1.8s">
                    	<td class="number-td light-blue-text"><img src="img/family-office.png" class="icon-css" alt="Family Office" title="Family Office"></td>
                        <td class="text-td dark-blue-text">Family Office</td>
                    </tr>
                	<tr class="wow fadeIn ani23" data-wow-delay="2.3s">
                    	<td class="number-td light-blue-text"><img src="img/angel-investor.png" class="icon-css" alt="Angel Investor" title="Angel Investor"></td>
                        <td class="text-td dark-blue-text">Angel Investor</td>
                    </tr>                    
                	<tr class="wow fadeIn ani24" data-wow-delay="2.8s">
                    	<td class="number-td light-blue-text"><img src="img/government-grants.png" class="icon-css" alt="Government Grants" title="Government Grants"></td>
                        <td class="text-td dark-blue-text">Government Grants</td>
                    </tr>                     
                </tbody>
            </table>
        </div>         
</div>
<div class="clear"></div>
<div class="same-padding width100 white-spacing-div overflow" id="testimonial">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani25" data-wow-delay="0.3s">Testimonial</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani25" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <div class="five-col float-left two-column-css">
        	<h1 class="light-blue-text lato border-text small-border-text wow fadeIn ani26" data-wow-delay="0.9s"><b>We have helped a start-up get funded from ideation to closing of its funds</b></h1>
            <div class="short-blue-border wow fadeIn ani26" data-wow-delay="1.4s"></div>
            <p class="contact-p light-blue-text tav wow fadeIn ani27" data-wow-delay="1.9s">
                Name of Start-up<br>
                <b class="dark-blue-text weight900">PENTAIP (M) SDN. BHD.[170552-P]</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn ani28" data-wow-delay="2.4s">
                Amount Raised<br>
                <b class="dark-blue-text weight900 big-b">RM1.9 million</b>
                <br><span class="tav dark-blue-text lighter-text">(Despite in RMCO conditions)</span>
        	</p>            
        </div>	
        <div class="five-col float-right two-column-css">
        	<img src="img/funding.jpg" class="width100 funding-img wow fadeIn ani29" data-wow-delay="2.9s">
            <p ><a class="source-p light-blue-text tav light-blue-hover wow fadeIn ani30" data-wow-delay="3.4s" href="https://www.crowdplus.asia/CapitalProfile.php?cid=6789" target="_blank">Source: CrowdPlus.asia</a></p>
        </div> 
</div>
<div class="clear"></div>
<!--<div class="same-padding width100 white-spacing-div overflow" id="raise">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani31" data-wow-delay="0.3s">VinCaps Raise</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani31" data-wow-delay="0.5s"></div>
		<div class="clear"></div>
        <h1 class="price-h1 lato dark-blue-text wow fadeIn ani32" data-wow-delay="0.8s"><img src="img/vincaps-logo.png" class="price-logo" alt="VinCaps" title="VinCaps"> Price List</h1>
        <div class="clear"></div>
        <table class="price-table lato wow fadeIn ani33" data-wow-delay="1.2s">
        	<thead>
                <tr>
                	<th class="th1 title-td"></th>
                    <th class="th2 title-td">Startup</th>
                    <th class="th3 title-td">SME</th>
                    <th class="th4 title-td">Corporation</th>
                </tr>
            </thead>
        	<tbody>
                <tr>
                	<td class="td1 content-td">Company Assessment Form</td>
                    <td class="td1 content-td2">RM500</td>
                    <td class="td1 content-td2">RM500</td>
                    <td class="td1 content-td2">RM500</td>
                </tr>
                <tr>
                	<td class="td2 content-td">Pitch Deck Preparation</td>
                    <td class="td2 content-td2">RM300</td>
                    <td class="td2 content-td2">RM600</td>
                    <td class="td2 content-td2"  rowspan="6">Request for Quotation</td>
                </tr> 
                <tr>
                	<td class="td1 content-td">Presentation Coaching</td>
                    <td class="td1 content-td2">RM500</td>
                    <td class="td1 content-td2">RM1,000</td>
                    
                </tr>
                <tr>
                	<td class="td2 content-td">Company Profile</td>
                    <td class="td2 content-td2">RM200</td>
                    <td class="td2 content-td2">RM500</td>
                    
                </tr>   
                <tr>
                	<td class="td1 content-td">Corporate Branding</td>
                    <td class="td1 content-td2">RM600</td>
                    <td class="td1 content-td2">RM1,500</td>
                    
                </tr>                  
                <tr>
                	<td class="td2 content-td">Fact Sheet</td>
                    <td class="td2 content-td2">RM300</td>
                    <td class="td2 content-td2">RM600</td>
                    
                </tr>                  
                <tr>
                	<td class="td1 content-td">Professional Business Plan</td>
                    <td class="td1 content-td2">RM500</td>
                    <td class="td1 content-td2">RM1,000</td>
                    
                </tr>
                <tr>
                	<td class="td2 content-td">Consultancy Services</td>
                    <td class="td2 content-td2" colspan="3">RM150 per hour*</td>
                </tr>                  
                                                                                
            </tbody>
        </table>
</div>        -->
<div class="clear"></div>       

  

<div class="same-padding width100 white-spacing-div overflow" id="contactus">
        
        <p class="blue-text lato top-des float-left ow-section-des wow fadeIn ani31" data-wow-delay="0.3s">Contact Us</p>
       
        <div class="line-startup ow-section-line float-right wow fadeIn ani31" data-wow-delay="0.5s"></div>
		<div class="clear"></div>       
        <div class="five-col float-left two-column-css">
   	<form id="contactform" method="post" action="index.php" class="form-class extra-margin">
        <input type="text" name="name" placeholder="Name" class="input-name clean dark-blue-text wow fadeIn ani32" data-wow-delay="0.8s" required >

                  <input type="email" name="email" placeholder="Email" class="input-name clean lato dark-blue-text wow fadeIn ani33" data-wow-delay="1.4s" required >

                  <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean lato dark-blue-text wow fadeIn ani34" data-wow-delay="2.0s" required >



                  <textarea name="comments" placeholder="Message" class="input-name input-message clean lato dark-blue-text wow fadeIn ani35" data-wow-delay="2.6s" ></textarea>
                  <div class="clear"></div>
                  <table class="form-table">
                  	<tbody>
                        <tr class="wow fadeIn ani36" data-wow-delay="3.2s">
                            <td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 clean lato" required></td>
                            <td><p class="opt-msg lato dark-blue-text">I want to be updated with more information about your company's news and future promotions</p></td>
                        </tr>
                        <tr class="wow fadeIn ani37" data-wow-delay="3.8s">
                            <td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 clean lato"  required></td>
                            <td><p class="opt-msg lato dark-blue-text"> I just want to be contacted based on my request/inquiry</p></td>
                        </tr>
                    </tbody>
                  </table>

                  <div class="res-div"><input type="submit" name="send_email_button" value="Send" class="input-submit blue-button white-text clean pointer lato wow fadeIn ani38" data-wow-delay="4.4s"></div>


        </form>
            
        </div>	
        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text tav wow fadeIn ani39" data-wow-delay="4.9s">
                Contact Number<br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60165324691">+6016-532 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn ani40" data-wow-delay="5.4s">
                Email Address<br>
                <b class="dark-blue-text weight900">kevin.vincaps@gmail.com</b>
        	</p>
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn ani41" data-wow-delay="5.9s" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn ani42" data-wow-delay="6.4s" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
        </p>                        
        </div> 
</div>
<?php include 'js.php'; ?>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "kevin.vincaps@gmail.com, sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via VinCaps website";

    function died($error)
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }


    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }



    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments'];
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;

	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }


    $string_exp = "/^[A-Za-z .'-]+$/";

  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }




  if(strlen($error_message) > 0) {
    died($error_message);
  }

    $email_message = "Form details below.\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }

    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
echo '<script>alert("Thank for messaging us. You can visit our Facebook page for more details while waiting for the reply.")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php

}
?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>