<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

$pendingMember = getPayment($conn, "WHERE status = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/reject.php" />
<link rel="canonical" href="https://vincaps.com/reject.php" />
<meta property="og:title" content="VinCaps | Rejected" />
<title>VinCaps | Rejected</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

    <h1 class="price-h1 dark-blue-text lato">Premium Membership (Rejected) | <a href="adminDashboard.php" class="light-blue-link">Pending</a> | <a href="approve.php" class="light-blue-link">Approved</a></h1>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">
  
        <table class="approve-table lato">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Full Name</th>
                        <th>Company Name</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Bank in To</th>
                        
                        <th>Payment Method</th>
                        <th>Account Holder Name</th>
                        <th>Bank Reference</th>
                        <th>Receipt</th>
                        <th>Date</th>
                        <th>Approval</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($pendingMember)
                    {
                        for($cnt = 0;$cnt < count($pendingMember) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $pendingMember[$cnt]->getUsername();?></td>
                                <td>Company Name</td>
                                <td>Contact</td>
                                <td>Email</td>
                                <td>bank_selection</td>
                                <td>package_selection</td>
                                <td>bank_holder</td>
                                <td><?php echo $pendingMember[$cnt]->getBankReference();?></td>
                                <td><a href="./img/receipt.pdf" data-fancybox="images-preview"><img src="img/receipt.png" class="receipt-icon opacity-hover" alt="Receipt" title="Receipt"></a></td>
                                <td><?php echo $pendingMember[$cnt]->getDateCreated();?></td>

                                <td>
                                    <form method="POST" action="utilities/adminApprovedSubscribeFunction.php" class="hover1">
                                        <button class="clean transparent-button left-button" type="submit" name="transfer_uid" value="<?php echo $pendingMember[$cnt]->getUid();?>">
                                            <img src="img/approve.png" class="approval-icon" alt="Approve" title="Approve">
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

</div>

<?php include 'js.php'; ?>

</body>
</html>