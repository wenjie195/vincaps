<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
// require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
// $uid = $_SESSION['uid'];

$conn = connDB();

$articles = getArticles($conn,"WHERE article_link = ? AND display = 'YES' ",array("article_link"),array($_GET['id']), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<!-- <head> -->

<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$articlesDetails = getArticles($conn,"WHERE article_link = ? ", array("article_link") ,array($_GET['id']),"s");
?>

    <?php
    if($articlesDetails)
    {
        for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
        {
        ?>

            <head>
            <meta property="og:image" content="https://vincaps.com/uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();;?>" />
            <?php include 'meta.php'; ?>
            <meta property="og:image" content="https://vincaps.com/uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();;?>" />
            <meta property="og:title" content="<?php echo $articlesDetails[$cnt]->getTitle();?> | VinCaps" />
            
            
            <meta property="og:description" content="<?php echo $articlesDetails[$cnt]->getKeywordOne();?>" />
            <meta name="description" content="<?php echo $articlesDetails[$cnt]->getKeywordOne();?>" />
            
            <meta name="keywords" content="<?php echo $articlesDetails[$cnt]->getKeywordTwo();?>">
            <!--<link rel="canonical" href="https://chillitbuddy.com/" />-->
            <link rel="canonical" href="https://vincaps.com/blog.php?id=<?php echo $articlesDetails[$cnt]->getArticleLink();?>" />
            <title><?php echo $articlesDetails[$cnt]->getTitle();?> | VinCaps</title>            
            
            
            
            <?php include 'css.php'; ?>
            </head>

        <?php
        }
        ?>
    <?php
    }
    ?>

    <?php
    if($articlesDetails)
    {
        for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
        {
        ?>
            <body class="body">
            <?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
            <?php include 'header.php'; ?>
            <div class="background-div">
                <div class="cover-gap content min-height2 blog-content-div1">
                    <div class="print-area" id="printarea">

                        <div class="clear"></div>

                        <h1 class="red-h1 slab darkpink-text"><?php echo $articlesDetails[$cnt]->getTitle();?></h1>

                        <div class="">
                            <p class="blue-date">By <?php echo $articlesDetails[$cnt]->getAuthorName();?> | <?php echo $date = date("d-m-Y",strtotime($articlesDetails[$cnt]->getDateCreated()));?></p>
                        </div>   

                        <img src="uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();;?>" class="width100 cover-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
                        <p class="source-p"><?php echo _BLOG_SOURCE ?> : <?php echo $articlesDetails[$cnt]->getImgCoverSrc();?></p>
                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphOne();?></p>    
                                <!-- <p class="article-p">Author: <b><?php //echo $reviewWriter = $articlesDetails[$cnt]->getAuthorName();?> -->
                                <p class="article-p"><?php echo _BLOG_AUTHOR ?>: <b><?php echo $articlesDetails[$cnt]->getAuthorName();?></b>
                                    <img src="img/feather.png" class="feather-png" alt="Author" title="Author">
                                </p>
                            </div>
                        </div>
                        
                        <div class="hide-print">
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->

                            <div class="clear"></div>

                            <div class="width100 overflow padding20">
                                <!-- AddToAny BEGIN -->
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3"><?php echo _BLOG_SHARE ?>:</h3>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_copy_link"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_blogger"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_wechat"></a>
                                <a class="a2a_button_line"></a>
                                <a class="a2a_button_telegram"></a>
                                <!--<a class="a2a_button_print"></a>-->
                                </div> 
                            </div>



                            <div class="width100 overflow padding20">
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3"><?php echo _BLOG_RECOMMEND ?>:</h3>
                                <?php
                                    $currentUid = $articles[$cnt]->getUid();
                                    $allArticles = getArticles($conn," WHERE uid != '$currentUid' AND display = 'YES' ORDER BY date_created DESC LIMIT 3");
                                    if($allArticles)
                                    {   
                                        for($cntAA = 0;$cntAA < count($allArticles) ;$cntAA++)
                                        {
                                        ?>
                                        <a href='blog.php?id=<?php echo $allArticles[$cntAA]->getArticleLink();?>'>
                                            <div class="article-card article-card-overwrite recommended opacity-hover">
                                                <div class="article-bg-img-box">
                                                    <img src="uploadsArticle/<?php echo $allArticles[$cntAA]->getTitleCover();?>" class="article-img1" alt="<?php echo $allArticles[$cntAA]->getTitle();?>" title="<?php echo $allArticles[$cntAA]->getTitle();?>">
                                                </div>
                                                <div class="box-caption box2">
                                                    <div class="wrap-a wrap100">
                                                        <!--<span class="peach-text"><?php echo $allArticles[$cntAA]->getType();?></span>-->
                                                        <span class="light-blue-text small-date"><?php echo $allArticles[$cntAA]->getDateCreated();?></span>
                                                    </div>
                                                    <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a text-overflow">
                                                        <?php echo $allArticles[$cntAA]->getTitle();?>
                                                    </div>
                                                    <div class="text-content-div">
                                                        <?php echo $description = $allArticles[$cntAA]->getKeywordOne();?>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>   
                                        <?php
                                        }
                                        ?>
                                    <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
            </body>
        <?php
        }
        ?>
    <?php
    }
    ?>

<?php
}
?>

<?php include 'js.php'; ?>

<script>
document.onkeydown = function(e) {
    if(e.keyCode == 123) {
      return false;
     }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
     return false;
    }
	 if(e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }      
 }
</script>

<style>
*{
    color:#3b6a94;
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}

</style>

<!-- </body> -->
</html>