<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $pendingMember = getPayment($conn, "WHERE status = 'Pending' ");
$allArticles = getAnnouncement($conn, " WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/adminAnnouncementView.php" />
<link rel="canonical" href="https://vincaps.com/adminAnnouncementView.php" />
<meta property="og:title" content="VinCaps | Admin Announcement" />
<title>VinCaps | Admin Announcement</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

    <h1 class="price-h1 dark-blue-text lato">Announcement</h1>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">
  
        <table class="approve-table lato">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allArticles)
                    {
                        for($cnt = 0;$cnt < count($allArticles) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allArticles[$cnt]->getTitle();?></td>
                                <td><?php echo $allArticles[$cnt]->getDateCreated();?></td>

                                <td>
                                    <!-- <form action="adminBlogEdit.php" method="POST"> -->
                                    <form action="adminAnnouncementEdit.php" method="POST">
                                        <button class="clean blue-link transparent-button pointer" type="submit" name="item_uid" value="<?php echo $allArticles[$cnt]->getUid();?>">Edit</button>
                                    </form>
                                </td>

                                <td>
                                    <form method="POST" action="utilities/adminAnnouncementDeleteFunction.php" class="hover1">
                                        <button class="clean transparent-button" type="submit" name="item_uid" value="<?php echo $allArticles[$cnt]->getUid();?>">
                                            <img src="img/reject.png" class="approval-icon opacity-hover pointer" alt="Delete" title="Delete">
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Announcement Added !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to ADD Announcement !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR to Add Announcement !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    elseif($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Announcement Deleted !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to DELETE Announcement !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR to Delete Announcement !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    elseif($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Announcement Updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to UPDATE Announcement !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR to Update Announcement !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>