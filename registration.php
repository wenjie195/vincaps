<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/registration.php" />
<link rel="canonical" href="https://vincaps.com/registration.php" />
<meta property="og:title" content="VinCaps | Registration" />
<title>VinCaps | Registration</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1" style="padding-top:70px !important;">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Register Now</h1>
	<div class="teh-border blue-border margin-auto wow fadeIn border-margin" data-wow-delay="0.4s"></div>
    <p class="teh-all-desc light-blue-text wow fadeIn res-width last-cen" data-wow-delay="0.6s">To get RM 100 off voucher on our Product and Service !</p>
        <div class="five-col float-left two-column-css">

        <form method="POST" action="utilities/registrationFunction.php">
            <input type="text" placeholder="Name" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" data-wow-delay="0.8s" required >
            <input type="email" placeholder="Email" id="email" name="email" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1s" required >
            <input type="text" type="text" placeholder="Contact No." id="phone" name="phone" class="input-name clean lato dark-blue-text wow fadeIn" data-wow-delay="1.2s" required >
            <div class="res-div"><input type="submit" name="submit" value="Register" class="input-submit blue-button white-text clean pointer lato wow fadeIn" data-wow-delay="1.4s" style="margin-bottom:30px;"></div>
        </form>
            
        </div>	
        <div class="five-col float-right two-column-css">
			<p class="contact-p light-blue-text tav wow fadeIn" data-wow-delay="1.6s" style="margin-top:0 !important;">
                For inquiries, may approach us on <br>Contact Number<br>
                <b class="dark-blue-text weight900"><a class="dark-blue-text weight900" href="tel:+60165324691">+6016 532 4691</a> (Kevin Yam)</b>
        	</p> 
            <p class="contact-p light-blue-text tav wow fadeIn" data-wow-delay="1.8s">
                Email Address<br>
                <b class="dark-blue-text weight900">hello.vincaps@gmail.com</b>
        	</p>
            <p class="contact-p light-blue-text tav wow fadeIn" data-wow-delay="2s">
                Address<br>
                <b class="dark-blue-text weight900">
1-3-07&08, Summerskye Commercial Square, Jalan Sungai Tiram, 11900 Bayan Lepas, Pulau Pinang, Malaysia</b>
        	</p>            
            
        <p class="contact-p lighter-blue-text tav">
        	<a href="https://bit.ly/VincapsService" target="_blank"><img src="img/whatsapp2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="2.2s" alt="VinCaps Whatsapp" title="VinCaps Whatsapp"></a>
        	<a href="https://www.facebook.com/vincapsmy" target="_blank"><img src="img/facebook2.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="2.4s" alt="VinCaps Facebook" title="VinCaps Facebook"></a>
            <a href="https://www.instagram.com/vincaps.capital/" target="_blank"><img src="img/insta.png" class="social-icon opacity-hover display-inline wow fadeIn" data-wow-delay="2.6s" alt="VinCaps Instagram" title="VinCaps Instagram"></a>
        </p>  
        <p class="contact-p lighter-blue-text tav wow fadeIn" data-wow-delay="2.8s"><a href="team.php" target="_blank" class=" blue-link">Check Out VinCaps Corporate Structure & Internal Team</a></p>                      
        </div> 
</div>
<div class="clear"></div>  
<div class="width100 overflow">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy" class="google-iframe wow fadeIn" data-wow-delay="3s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>

<?php include 'js.php'; ?>
<?php unset($_SESSION['registration_uid']);?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>