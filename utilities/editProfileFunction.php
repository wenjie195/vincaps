<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $fullname = rewrite($_POST["update_fullname"]);
    //extra update
    $username = rewrite($_POST["update_fullname"]);
    $email = rewrite($_POST["update_email"]);
    $phone = rewrite($_POST["update_phone"]);
    $companyName = rewrite($_POST["update_companyname"]);

    //   FOR DEBUGGING 
    // echo "<br>";
    // echo $fullname."<br>";
    // echo $register_email."<br>";
    // echo $register_contact."<br>";

    $user = getUser($conn," uid = ?  ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($fullname)
        {
            array_push($tableName,"fullname");
            array_push($tableValue,$fullname);
            $stringType .=  "s";
        }
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($companyName)
        {
            array_push($tableName,"company_name");
            array_push($tableValue,$companyName);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../editProfile.php?type=1');
            echo "<script>alert('Update Profile success !');window.location='../editProfile.php'</script>";
        }
        else
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../editProfile.php?type=2');
            echo "fail";
        }
    }
    else
    {
        // $_SESSION['messageType'] = 1;
        // header('Location: ../editProfile.php?type=3');
        echo "error";
    }

}
else 
{
    header('Location: ../index.php');
}
?>