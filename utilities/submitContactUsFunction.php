<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Package.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];

function registerPackage($conn,$uid,$name,$email,$phone,$selectionType,$comments,$contactOption,$newsletter,$status)
{
     if(insertDynamicData($conn,"package",array("uid","username","email","phone","package","message","selection","newsletter","status"),
          array($uid,$name,$email,$phone,$selectionType,$comments,$contactOption,$newsletter,$status),"sssssssss") === null)
     {    }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $name = rewrite($_POST['name']);
     $email = rewrite($_POST['email']);
     $phone = rewrite($_POST['telephone']);
     $selectionType = rewrite($_POST['selection']);
     $comments = rewrite($_POST['comments']);
     $contactOption = rewrite($_POST['contact-option']);

     if($contactOption == "contact-more-info")
     {
          $newsletter = "Yes";
     }
     elseif($contactOption == "contact-on-request")
     // else
     {
          $newsletter = "No";
     }

     $status = "Display";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_username."<br>";
     // echo $register_email."<br>";
     // echo $register_contact."<br>";

     if(registerPackage($conn,$uid,$name,$email,$phone,$selectionType,$comments,$contactOption,$newsletter,$status))
     {

          // header('Location: ../ThankYouRenew.php');

          $email_to = "hello.vincaps@gmail.com";
          $email_subject = "Contact Form via Vincaps website";

          // $first_name = $_POST['name']; // required
          // $email_from = $_POST['email']; // required
          // $telephone = $_POST['phone']; //required
          // $comments = $_POST['comments']; // required
          // $contactOption = $_POST['contact-option']; // required

          $first_name = rewrite($_POST['name']);
          $email_from = rewrite($_POST['email']);
          $telephone = rewrite($_POST['telephone']);
          $comments = rewrite($_POST['comments']);
		  $selection = rewrite($_POST['selection']);
          $contactOption = rewrite($_POST['contact-option']);
          $contactMethod = null;

          if($contactOption == null || $contactOption == "")
          {
               $contactMethod = "don\'t bother me";
          }
          else if($contactOption == "contact-more-info")
          {
               $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
          }
          else if($contactOption == "contact-on-request")
          {
               $contactMethod = "I just want to be contacted based on my request/ inquiry";
          }
          else
          {
               $contactMethod = "error getting contact options";
               $error_message .="Error getting contact options\n\n";
          }

          $error_message = "";
          $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

          if(!preg_match($email_exp,$email_from)) 
          {
               $error_message .= 'The email address you entered does not appear to be valid.\n';
          }

          $string_exp = "/^[A-Za-z .'-]+$/";

          if(!preg_match($string_exp,$first_name)) 
          {
               $error_message .= 'The name you entered does not appear to be valid.\n';
          }

          // if(strlen($comments) < 2) 
          // {
          //      $error_message .= 'The message you entered do not appear to be valid.\n';
          // }

          if(strlen($error_message) > 0) 
          {
               died($error_message);
          }

          $email_message = "Form details below.\n\n";


          function clean_string($string) 
          {
               $bad = array("content-type","bcc:","to:","cc:","href");
               return str_replace($bad,"",$string);
          }

          $email_message .= "Name: ".clean_string($first_name)."\n";
          $email_message .= "Email: ".clean_string($email_from)."\n";
          $email_message .= "Telephone: ".clean_string($telephone)."\n";
		  $email_message .= "Status : ".clean_string($selection)."\n";
          $email_message .= "Message : ".clean_string($comments)."\n";
          $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";
          // $email_message .= "Contact Option : ".clean_string($contactOption)."\n";
          // $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

          // create email headers
          $headers = 'From: '.$email_from."\r\n".
          'Reply-To: '.$email_from."\r\n" .
          'X-Mailer: PHP/' . phpversion();
          @mail($email_to, $email_subject, $email_message, $headers); 

          // $_SESSION['messageType'] = 1;
          // header('Location: ../index.php?type=1');
          // echo "your email has been submit";
          // echo "<script>alert('Your Request has been Submited');window.location='../index.php'</script>";
          header('Location: ../ThankYouRenew.php');
     }     
}
else 
{
     // header('Location: ../addReferee.php');
     header('Location: ../index.php');
}
?>