<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["item_uid"]);
    $display = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $display_Type."<br>";

    if(isset($_POST['item_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $statusUpdate = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($statusUpdate)
        {
            // echo "success";
            // header('Location: ../adminAnnouncementView.php?type=1');
            $_SESSION['messageType'] = 2;
            header('Location: ../adminAnnouncementView.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../adminAnnouncementView.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 2;
        header('Location: ../adminAnnouncementView.php?type=3');
    }

}
else
{
     header('Location: ../index.php');
}
?>