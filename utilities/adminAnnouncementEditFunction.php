<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];
$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);
     $title = rewrite($_POST['update_title']);
     $seoTitle = $title;

     $oriFileOne = rewrite($_POST["original_photo"]);
     $newFileOne = $_FILES['update_cover_photo']['name'];
     if($newFileOne == '')
     {
         $coverPhoto = $oriFileOne;
     }
     else
     {
          // $file = $oriFileOne;
          $coverPhoto = $timestamp.$_FILES['update_cover_photo']['name'];
          $target_dir = "../uploadsArticle/";
          $target_file = $target_dir . basename($_FILES["update_cover_photo"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          // $extensions_arr = array("jpg","jpeg","png","gif","pdf");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['update_cover_photo']['tmp_name'],$target_dir.$coverPhoto);
          }
     }


     // $imgCoverSrc = rewrite($_POST['update_cover_photo_source']);
     $imgCoverSrcData = rewrite($_POST['update_photo_source']);
     if($imgCoverSrcData ==  '')
     {
          $imgCoverSrc = "No";
     }
     else
     {
          $imgCoverSrc = rewrite($_POST['update_photo_source']);
     }

     // $keywordOne = rewrite($_POST['update_summary']);
     $keywordOne = ($_POST['update_summary']); 
     $paragraphOne = ($_POST['editor']);  //no rewrite, cause error in db

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($seoTitle)
          {
               array_push($tableName,"seo_title");
               array_push($tableValue,$seoTitle);
               $stringType .=  "s";
          }
          if($coverPhoto)
          {
               array_push($tableName,"title_cover");
               array_push($tableValue,$coverPhoto);
               $stringType .=  "s";
          }
          if($imgCoverSrc)
          {
               array_push($tableName,"img_cover_source");
               array_push($tableValue,$imgCoverSrc);
               $stringType .=  "s";
          }
          if($keywordOne)
          {
               array_push($tableName,"keyword_one");
               array_push($tableValue,$keywordOne);
               $stringType .=  "s";
          }
          if($paragraphOne)
          {
               array_push($tableName,"paragraph_one");
               array_push($tableValue,$paragraphOne);
               $stringType .=  "s";
          }          
          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 3;
               header('Location: ../adminAnnouncementView.php?type=1');
          }
          else
          {    
               $_SESSION['messageType'] = 3;
               header('Location: ../adminAnnouncementView.php?type=2');
          }
     }
     else
     {
          $_SESSION['messageType'] = 3;
          header('Location: ../adminAnnouncementView.php?type=3');
     }
}
else 
{
     header('Location: ../index.php');
}

?>