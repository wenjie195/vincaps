<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$currentUserUid = $_SESSION['uid'];

$timestamp = time();

function subscribePackage($conn,$uid,$userUid,$username,$email,$phone,$package,$reference,$file,$status,$bank,$bankHolder,$companyName)
{
     if(insertDynamicData($conn,"payment",array("uid","user_uid","username","email","phone_no","package","bank_reference","receipt","status","bank","bank_holder","company_name"),
          array($uid,$userUid,$username,$email,$phone,$package,$reference,$file,$status,$bank,$bankHolder,$companyName),"ssssssssssss") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     
     $getUserDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($currentUserUid),"s");
     $userUid = $currentUserUid;
     // $username = $getUserDetails[0]->getUsername();
     $username = $getUserDetails[0]->getFullname();
     $email = $getUserDetails[0]->getEmail();
     $phone = $getUserDetails[0]->getPhoneNo();
     $companyName = $getUserDetails[0]->getCompanyName();

     // $amount = rewrite($_POST['amount']);
     $package = rewrite($_POST['package_selection']);
     $reference = rewrite($_POST['reference']);
     $bank = rewrite($_POST['bank_selection']);
     $bankHolder = rewrite($_POST['bank_holder']);

     $file = $timestamp.$_FILES['file_one']['name'];
     $target_dir = "../receipt/";
     $target_file = $target_dir . basename($_FILES["file_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif","pdf");
     if( in_array($imageFileType,$extensions_arr) )
     {
         move_uploaded_file($_FILES['file_one']['tmp_name'],$target_dir.$file);
     }

     $status = 'Pending';

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $user."<br>";

     if(subscribePackage($conn,$uid,$userUid,$username,$email,$phone,$package,$reference,$file,$status,$bank,$bankHolder,$companyName))
     {  
          // echo "<script>alert('data submitted, please wait for 2 working days');window.location='../subscribe.php'</script>";
          // echo "success";
          header('Location: ../thankyou.php');
     }
     else
     {
          echo "ERROR";
     }   
}
else 
{
     header('Location: ../index.php');
}
?>