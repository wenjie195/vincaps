<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $transfer_uid = rewrite($_POST["transfer_uid"]);
    $type = "2";
    $status = "Approved";

    $aaa = getPayment($conn," WHERE uid = ? ",array("uid"),array($transfer_uid),"s");   
    $userUid = $aaa[0]->getUserUid();

    // // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $transfer_uid."<br>";
    // echo $userUid."<br>";

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $currentTime = $dt->format('Y-m-d');

    // echo $Date1 = '2010-09-17';
    $Date1 = $currentTime;
    $date = new DateTime($Date1);
    // $additional = $duration;
    $additional = "365 days";
    // echo $additional = "10 days";
    // $date->modify('+1 day');
    $date->modify($additional);

    $duration = $additional;
    $manufactured = $Date1;

    $expired = $date->format('Y-m-d');

    $upgradeMembership = getPayment($conn," WHERE uid = ? ",array("uid"),array($transfer_uid),"s");   
    // $userUid = $upgradeMembership[0]->getUserUid();

    if($upgradeMembership)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($duration)
        {
            array_push($tableName,"duration");
            array_push($tableValue,$duration);
            $stringType .=  "s";
        }
        if($manufactured)
        {
            array_push($tableName,"manufactured");
            array_push($tableValue,$manufactured);
            $stringType .=  "s";
        }
        if($expired)
        {
            array_push($tableName,"expired");
            array_push($tableValue,$expired);
            $stringType .=  "s";
        }
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        if($type)
        {
            array_push($tableName,"user_type");
            array_push($tableValue,$type);
            $stringType .=  "s";
        }

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            // echo "<script>alert('membership upgraded !!');window.location='../adminDashboard.php'</script>";  

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            if($duration)
            {
                array_push($tableName,"duration");
                array_push($tableValue,$duration);
                $stringType .=  "s";
            }
            if($manufactured)
            {
                array_push($tableName,"manufactured");
                array_push($tableValue,$manufactured);
                $stringType .=  "s";
            }
            if($expired)
            {
                array_push($tableName,"expired");
                array_push($tableValue,$expired);
                $stringType .=  "s";
            }

            if($status)
            {
                array_push($tableName,"status");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }
    
            array_push($tableValue,$transfer_uid);
            $stringType .=  "s";
            $statusUpdated = updateDynamicData($conn,"payment"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($statusUpdated)
            {
                echo "<script>alert('membership upgraded !!');window.location='../adminMembershipPending.php'</script>";  
            }
            else
            {
                echo "<script>alert('fail to delete annoucement !!');window.location='../adminMembershipPending.php'</script>";  
            }

        }
        else
        {
            echo "<script>alert('fail to delete annoucement !!');window.location='../adminMembershipPending.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminMembershipPending.php'</script>";  
    }

}
else 
{
    header('Location: ../index.php');
}
?>