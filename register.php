<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="VinCaps | Your Startup Funding Consultant" />
<title>VinCaps | Your Startup Funding Consultant</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="big-div width100 overflow">
	<div class="width100 same-padding">


        <form method="POST"  action="utilities/registerFunction.php">

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Username</p>
                <input class="input-name" type="text" placeholder="Username" id="register_username" name="register_username" required>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Email</p>
                <input class="input-name" type="email" placeholder="Email" id="register_email" name="register_email" required>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Contact</p>
                <input class="input-name" type="text" placeholder="Contact" id="register_contact" name="register_contact" required>
            </div>

            <div class="clear"></div>

            <div class="width100 text-center margin-top20">
                <button name="register" class="blue-button white-text clean pointer">REGISTER</button>
            </div>

        </form>    

              
    </div>
</div>