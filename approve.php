<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

$pendingMember = getPayment($conn, "WHERE status = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/approve.php" />
<link rel="canonical" href="https://vincaps.com/approve.php" />
<meta property="og:title" content="VinCaps | Approved Premium Member" />
<title>VinCaps | Approved Premium Member</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

    <h1 class="price-h1 dark-blue-text lato">Premium Membership (Approved) | <a href="adminDashboard.php" class="light-blue-link">Pending</a> | <a href="reject.php" class="light-blue-link">Rejected</a></h1>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">
  
        <table class="approve-table lato">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Full Name</th>
                        <th>Company Name</th>
                        <th>Contact</th>
                        <th>Receipt</th>
                        <th>Start Date</th>
                        <th>Expiry Date</th>
                        <th>Approval</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($pendingMember)
                    {
                        for($cnt = 0;$cnt < count($pendingMember) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $pendingMember[$cnt]->getUsername();?></td>
								<td>Company Name</td>
								<td>Contact</td>
                                <td><a href="./img/receipt.pdf" data-fancybox="images-preview"><img src="img/receipt.png" class="receipt-icon opacity-hover" alt="Receipt" title="Receipt"></a></td>
                                <td><?php echo $pendingMember[$cnt]->getDateCreated();?></td>
								<td>Expiry Date</td>
                                <td>
                                    <form method="POST" action="utilities/adminApprovedSubscribeFunction.php" class="hover1">

                                        <button class="clean transparent-button"><img src="img/reject.png" class="approval-icon" alt="Reject" title="Reject"></button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

</div>

<?php include 'js.php'; ?>

</body>
</html>