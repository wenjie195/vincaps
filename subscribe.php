<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="VinCaps | Your Startup Funding Consultant" />
<title>VinCaps | Your Startup Funding Consultant</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>


	<div class="width100 same-padding">

        <div class="dual-input">
            <p class="input-top-p ow-black-text">Bank Details</p>
        </div>

        <div class="dual-input">
            <p class="input-top-p ow-black-text">Bank : Maybank</p>
            <p class="input-top-p ow-black-text">Bank Account Holder : Vincaps</p>
            <p class="input-top-p ow-black-text">Account Number : 123112233</p>
        </div>

        <form action="utilities/subscribeFunction.php" method="POST" enctype="multipart/form-data">

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Amount</p>
                <input class="input-name" type="text" placeholder="Amount" id="amount" name="amount" required>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Package Selection</p>
                <!-- <input class="input-name" type="email" placeholder="Email" id="register_email" name="register_email" required> -->
                <select class="input-name" type="text" id="package_selection" name="package_selection" required>
                    <option value="" name=" ">PLEASE SELECT A PACKAGE TYPE</option>
                    <option value="Package 1" name="Package 1">Package 1</option>
                    <option value="Package 2" name="Package 2">Package 2</option>
                </select> 
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Receipt</p>
                <p><input id="file-upload" type="file" name="file_one" id="file_one" class="margin-bottom10 pointer" required/></p>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p ow-black-text">Reference</p>
                <input class="input-name" type="text" placeholder="Reference" id="reference" name="reference" required>
            </div>

            <div class="clear"></div>

            <div class="width100 text-center margin-top20">
                <button name="register" class="blue-button white-text clean pointer">SUBMIT</button>
            </div>

        </form>    

              
    </div>
